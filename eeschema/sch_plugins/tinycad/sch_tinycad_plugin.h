#ifndef _SCH_TINYCAD_PLUGIN_H_
#define _SCH_TINYCAD_PLUGIN_H_

#include <sch_line.h>
#include <sch_io_mgr.h>
#include <wx/filename.h>
//#include <plugins/tinycad/tinycad_parser.h>
#include <lib_item.h>
#include <list>
#include <geometry/seg.h>

#include <boost/ptr_container/ptr_map.hpp>


class EDA_TEXT;
class KIWAY;
class LINE_READER;
class SCH_SCREEN;
class SCH_SHEET;
class SCH_BITMAP;
class SCH_JUNCTION;
class SCH_NO_CONNECT;
class SCH_LINE;
class SCH_LABEL;
class SCH_BUS_ENTRY_BASE;
class SCH_BUS_WIRE_ENTRY;
class SCH_TEXT;
class SCH_GLOBALLABEL;
class SCH_SYMBOL;
class SCH_FIELD;
class PROPERTIES;
class LIB_SYMBOL;
class PART_LIB;
class LIB_CIRCLE;
class LIB_FIELD;
class LIB_RECTANGLE;
class LIB_POLYLINE;
class LIB_PIN;
class LIB_TEXT;
class LIB_ID;
class wxXmlNode;
class SCH_TINYCAD_PLUGIN_CACHE;
class SCH_TINYCAD_SYMBOL;
class SCH_TINYCAD_SYMBOL_COLLECTION;
enum SHEET_TYPE
{
    ROOT,
    SUB_PAGE,
    HIERARCHICAL
};

// should use wxPoint
typedef struct POS
{
    double x;
    double y;
} POS;

typedef std::map<wxString, std::vector<wxXmlNode*>> NODE_VECTOR_MAP;

typedef struct textFont
{
    int  size;
    bool bold;
    bool italic;
};

typedef struct lineStyle
{
    int            width;
    PLOT_DASH_TYPE style;
};

//NODE_VECTOR_MAP MapListChildren( wxXmlNode* aCurrentNode );

class SCH_TINYCAD_PLUGIN : public SCH_PLUGIN
{
public:
    SCH_TINYCAD_PLUGIN();
    ~SCH_TINYCAD_PLUGIN();

    const wxString GetName() const override;

    void SetReporter( REPORTER* aReporter ) override { m_reporter = aReporter; }

    const wxString GetFileExtension() const override;

    const wxString GetLibraryFileExtension() const override;

    int GetModifyHash() const override;

    SCH_SHEET* Load( const wxString& aFileName, SCHEMATIC* aSchematic,
                     SCH_SHEET*        aAppendToMe = nullptr,
                     const PROPERTIES* aProperties = nullptr ) override;

    void EnumerateSymbolLib( wxArrayString& aSymbolNameList, const wxString& aLibraryPath,
                             const PROPERTIES* aProperties = nullptr ) override;
    void EnumerateSymbolLib( std::vector<LIB_SYMBOL*>& aSymbolList, const wxString& aLibraryPath,
                             const PROPERTIES* aProperties = nullptr ) override;
    LIB_SYMBOL* LoadSymbol( const wxString& aLibraryPath, const wxString& aAliasName,
                            const PROPERTIES* aProperties = nullptr ) override;

    void SaveSymbol( const wxString& aLibraryPath, const LIB_SYMBOL* aSymbol,
                     const PROPERTIES* aProperties = nullptr ) override;

    void CreateSymbolLib( const wxString&   aLibraryPath,
                          const PROPERTIES* aProperties = nullptr ) override;

    bool DeleteSymbolLib( const wxString&   aLibraryPath,
                          const PROPERTIES* aProperties = nullptr ) override;
    void SaveLibrary( const wxString&   aLibraryPath,
                      const PROPERTIES* aProperties = nullptr ) override;


    bool IsSymbolLibWritable( const wxString& aLibraryPath ) override;

    bool CheckHeader( const wxString& aFileName ) override;

private:
    REPORTER* m_reporter;

    SCH_SHEET*                m_rootSheet;
    SCH_SHEET*                m_currentSheet;
    wxString                  m_version;
    wxFileName                m_filename;
    wxString                  m_libName;
    SCHEMATIC*                m_schematic;
    wxFileName                m_libraryFileName;
    SCH_SHEET_PATH            m_rootPath;
    SCH_TINYCAD_PLUGIN_CACHE* m_cache;

    //std::map<int, LIB_SYMBOL*> m_symbols;

    //std::map<wxString, LIB_SYMBOL*> m_symbols2;
    //std::map<wxString, SCH_TINYCAD_SYMBOL*> m_symbols3;
    SCH_TINYCAD_SYMBOL_COLLECTION* m_symbols;

    std::map<wxString, LIB_SYMBOL*> m_lib_symbols;

    SCH_PLUGIN::SCH_PLUGIN_RELEASER m_pi;
    std::unique_ptr<PROPERTIES>     m_properties;

    std::map<wxString, LIB_SYMBOL*> m_powerSymbols;

    std::map<wxString, LIB_SYMBOL*> m_powerSymbolsList;

    void            cacheLib( const wxString& aLibraryFileName, const PROPERTIES* aProperties );
    void            loadSheet( wxXmlNode* aSheetNode, SHEET_TYPE aSheetType,
                               std::set<wxString> aHierarchicalPins = std::set<wxString>() );
    void            loadPageInfo( wxXmlNode* aDetailsNode );
    void            saveSheet();
    void            setSheetFileName( SHEET_TYPE aSheetType );
    SCH_TEXT*       loadText( wxXmlNode* aTextNode, std::map<int, textFont*>* aTextFontMap );
    SCH_SYMBOL*     loadPower( wxXmlNode* aPowerNode );
    SCH_NO_CONNECT* loadNC( wxXmlNode* aNCNode );
    SCH_LINE*       loadWire( wxXmlNode* aWireNode );
    std::vector<SCH_LINE*> loadPolygoneLines( wxXmlNode*                 aPolygonNode,
                                              std::map<int, lineStyle*>* aLineStyleMap );
    std::vector<SCH_LINE*> loadRectangleLines( wxXmlNode*                 aRectangleNode,
                                               std::map<int, lineStyle*>* aLineStyleMap );
    SCH_LINE*              loadBus( wxXmlNode* aBusNode );
    SCH_BUS_WIRE_ENTRY*    loadBusEntry( wxXmlNode* aBusEntryNode );
    SCH_TEXT*              loadBusLabel( wxXmlNode* aBusLabelNode );
    SCH_TEXT*              loadLabel( wxXmlNode*         aLabelNode,
                                      std::set<wxString> aHierarchicalPins = std::set<wxString>() );
    SCH_TEXT*              loadPowerLabel( wxXmlNode* aPowerLabelNode );
    SCH_JUNCTION*          loadJunction( wxXmlNode* aJunctionNode );
    void loadFieldAttributes( SCH_SYMBOL* aComponent, std::vector<wxXmlNode*> aFields );
};

// From parser files

typedef std::unordered_map<wxString, wxXmlNode*> NODE_MAP;

/*
NODE_MAP MapChildren( wxXmlNode* aCurrentNode );

*/
// end from parser files

#endif