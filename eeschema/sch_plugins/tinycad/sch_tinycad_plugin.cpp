#include <iostream>

#include <sch_plugins/tinycad/sch_tinycad_plugin.h>

#include <locale_io.h>
#include <properties.h>

#include <algorithm>
#include <memory>
#include <wx/filename.h>
#include <wx/tokenzr.h>
#include <wx/wfstream.h>
#include <wx/xml/xml.h>

#include <symbol_library.h>
#include <plugins/eagle/eagle_parser.h>
#include <string_utils.h>
#include <lib_arc.h>
#include <lib_circle.h>
#include <lib_id.h>
#include <lib_item.h>
#include <lib_pin.h>
#include <lib_polyline.h>
#include <lib_rectangle.h>
#include <lib_text.h>
#include <project.h>
#include <sch_bus_entry.h>
#include <sch_symbol.h>
#include <project/net_settings.h>
#include <sch_edit_frame.h>
#include <sch_junction.h>
#include <sch_plugins/legacy/sch_legacy_plugin.h>
#include <sch_marker.h>
#include <sch_screen.h>
#include <sch_sheet.h>
#include <sch_no_connect.h>
#include <sch_sheet_path.h>
#include <sch_text.h>
#include <sch_sheet_pin.h>
#include <schematic.h>
#include <symbol_lib_table.h>
#include <wildcards_and_files_ext.h>
#include <page_info.h>
#include <title_block.h>

#include <boost/algorithm/string.hpp>

using namespace std;

enum ALIGNMENT
{
    CENTER,
    BOTTOMRIGHT
};

wxPoint  parsePosition( wxString aPos );
wxPoint  parseSymbolPosition( wxString aPos, wxString aCenterPos );
wxPoint  parseSymbolPosition( wxString aPos, wxPoint aCenterPos );
wxPoint  parseSymbolPosition( wxString aPos );
wxPoint  getCenter( LIB_SYMBOL* aPart, int aUnit );
wxPoint  roundToGrid( wxPoint aPoint );
int      roundToGrid( int aValue );
int      convertToEeschemaUnits( int aValue );
int      convertToEeschemaUnits( float aValue );
wxString getFieldValue( wxString aValue );
wxString getFieldName( wxString aValue );

//typedef std::map<wxString, LIB_SYMBOL*> LIB_SYMBOL_MAP;
typedef std::vector<LIB_SYMBOL*> LIB_SYMBOLS;

NODE_VECTOR_MAP MapVectorChildren( wxXmlNode* aCurrentNode )
{
    NODE_VECTOR_MAP nodesMap;

    if( aCurrentNode )
        aCurrentNode = aCurrentNode->GetChildren();

    while( aCurrentNode )
    {
        if( nodesMap.count( aCurrentNode->GetName() ) == 1 )
        {
            nodesMap[aCurrentNode->GetName()].push_back( aCurrentNode );
        }
        else
        {
            std::vector<wxXmlNode*> nodevector = { aCurrentNode };
            nodesMap[aCurrentNode->GetName()] = nodevector;
        }
        aCurrentNode = aCurrentNode->GetNext();
    }
    return nodesMap;
}

class SCH_TINYCAD_SYMBOL
{
public:
    SCH_TINYCAD_SYMBOL( LIB_SYMBOL* aSymbol = new LIB_SYMBOL( "placeholder symbol name" ),
                        wxString aSheetFileName = "", int aId = -1 );
    SCH_TINYCAD_SYMBOL( const SCH_TINYCAD_SYMBOL* aToBeDuplicated );
    ~SCH_TINYCAD_SYMBOL();
    void AlignSymbol( ALIGNMENT aAlignment );
    void FixBottomRight();

    void    SetTopLeft( wxPoint aPos, int aUnit, bool aForce = false );
    wxPoint GetTopLeft( int aUnit );

    void    SetBottomRight( wxPoint aPos, int aUnit, bool aForce = false );
    wxPoint GetBottomRight( int aUnit );

    void SetREF_POINT( wxPoint aPos, int aUnit );
    wxPoint GetREF_POINT(int aUnit);
    bool REF_POINTExists(int aUnit);

    LIB_SYMBOL*            m_symbol;
    wxString               m_sheetFileName;
    int                    m_id;
    std::map<int, wxPoint> m_bottomRight;
    wxPoint                m_temp_bottomRight;

    std::map<int, wxPoint> m_topLeft;
    wxPoint                m_temp_topLeft;

    std::map<int, wxPoint> m_REF_POINTs;

private:
};

class SCH_TINYCAD_SYMBOL_PARSER
{
public:
    static bool    LoadSymbol( wxXmlNode* aSymbolNode, SCH_TINYCAD_SYMBOL* aPart,
                               std::map<int, textFont*>*  aTextFontMap = nullptr,
                               std::map<int, lineStyle*>* aLineStyleMap = nullptr,
                               bool aLibFormat = false, wxXmlNode* aDetailsNode = nullptr );
    static wxPoint GetAlignmentOffset( LIB_SYMBOL* aPart, int aUnit, ALIGNMENT aAlignment );
    static textFont* LoadTextFont( wxXmlNode* aTextFontNode );

private:
    static void           loadFieldAttributes( LIB_SYMBOL* aPart, std::vector<wxXmlNode*> aFields );
    static LIB_PIN*       loadPin( SCH_TINYCAD_SYMBOL* aPart, wxXmlNode* aPinNode );
    static LIB_TEXT*      loadText( SCH_TINYCAD_SYMBOL* aPart, wxXmlNode* aTextNode,
                                    std::map<int, textFont*>* aTextFontMap );
    static LIB_RECTANGLE* loadRectangle( SCH_TINYCAD_SYMBOL* aPart, wxXmlNode* aRectNode,
                                         std::map<int, lineStyle*>* aLineStyleMap );
    static LIB_CIRCLE*    loadEllipse( SCH_TINYCAD_SYMBOL* aPart, wxXmlNode* aEllipseNode,
                                       std::map<int, lineStyle*>* aLineStyleMap );
    static std::vector<LIB_ITEM*> loadPolygon( SCH_TINYCAD_SYMBOL* aPart, wxXmlNode* aPolygonNode,
                                               std::map<int, lineStyle*>* aLineStyleMap );
    static LIB_POLYLINE*          loadPolyline( SCH_TINYCAD_SYMBOL* aPart, wxPoint aPos,
                                                std::vector<wxXmlNode*> aVertices );
    static LIB_ARC* loadArc( SCH_TINYCAD_SYMBOL* aPart, wxPoint aPos, wxXmlNode* aArcNodeA,
                             wxXmlNode* aArcNodeB );
    static wxPoint  getCenter( LIB_SYMBOL* aPart, int aUnit );
    static wxPoint  getBottomRight( LIB_SYMBOL* aPart, int aUnit );
    static void     snapPins( LIB_SYMBOL* aPart );
    static void     alignSymbol( LIB_SYMBOL* aPart, ALIGNMENT aAlignment );
};

class SCH_TINYCAD_SYMBOL_COLLECTION
{
public:
    SCH_TINYCAD_SYMBOL_COLLECTION();
    ~SCH_TINYCAD_SYMBOL_COLLECTION();
    void                AddSymbol( SCH_TINYCAD_SYMBOL* aSymbol );
    void                AddSymbol( LIB_SYMBOL* aSymbol, wxString aSheetFileName, int aId );
    void                AddIdToSymbol( wxString aSheetFileName, int aId, wxString aName );
    SCH_TINYCAD_SYMBOL* GetSymbol( wxString aSymbolName );
    SCH_TINYCAD_SYMBOL* GetSymbol( wxString aSheetFileName, int aId );
    bool                SymbolExists( wxString aName );

private:
    std::map<wxString, SCH_TINYCAD_SYMBOL*>      m_symbols_by_name;
    std::set<wxString>                           m_symbolNames;
    std::map<std::pair<wxString, int>, wxString> m_symbolName_from_id;
};

SCH_TINYCAD_SYMBOL::SCH_TINYCAD_SYMBOL( LIB_SYMBOL* aSymbol, wxString aSheetFileName, int aId )
{
    m_symbol = aSymbol;
    m_sheetFileName = aSheetFileName;
    m_id = aId;
}

SCH_TINYCAD_SYMBOL::~SCH_TINYCAD_SYMBOL()
{
}

SCH_TINYCAD_SYMBOL::SCH_TINYCAD_SYMBOL( const SCH_TINYCAD_SYMBOL* aToBeDuplicated )
{
    m_symbol = aToBeDuplicated->m_symbol->Duplicate();
    m_sheetFileName = aToBeDuplicated->m_sheetFileName;
    m_id = aToBeDuplicated->m_id;
    m_bottomRight = aToBeDuplicated->m_bottomRight;
    m_temp_bottomRight = aToBeDuplicated->m_temp_bottomRight;
    m_topLeft = aToBeDuplicated->m_topLeft;
    m_temp_topLeft = aToBeDuplicated->m_temp_topLeft;
}

void SCH_TINYCAD_SYMBOL::SetREF_POINT( wxPoint aPos, int aUnit )
{
    m_REF_POINTs[aUnit] = aPos;
}

wxPoint SCH_TINYCAD_SYMBOL::GetREF_POINT( int aUnit )
{
    return m_REF_POINTs.at(aUnit);
}

bool SCH_TINYCAD_SYMBOL::REF_POINTExists( int aUnit )
{
    return m_REF_POINTs.find(aUnit) != m_REF_POINTs.end();
}

void SCH_TINYCAD_SYMBOL::SetTopLeft( wxPoint aPos, int aUnit, bool aForce )
{
    if( m_topLeft[aUnit] != wxPoint( 0, 0 ) )
    {
        if( aPos.y > m_topLeft[aUnit].y || aForce )
        {
            m_topLeft[aUnit].y = aPos.y;
        }

        if( aPos.x < m_topLeft[aUnit].x || aForce )
            m_topLeft[aUnit].x = aPos.x;
    }
    else
    {
        m_topLeft[aUnit] = aPos;
    }
}

wxPoint SCH_TINYCAD_SYMBOL::GetTopLeft( int aUnit )
{
    if( m_topLeft.find( aUnit ) != m_topLeft.end() )
    {
        return m_topLeft[aUnit];
    }
    else
    {
        EDA_RECT bbox = m_symbol->GetBodyBoundingBox( aUnit, 0 );
        return roundToGrid( wxPoint( bbox.GetLeft(), bbox.GetTop() ) );
    }
}

wxPoint SCH_TINYCAD_SYMBOL::GetBottomRight( int aUnit )
{
    if( m_bottomRight.find( aUnit ) != m_bottomRight.end() )
    {
        return m_bottomRight[aUnit];
    }
    else
    {
        EDA_RECT bbox = m_symbol->GetBodyBoundingBox( aUnit, 0 );
        return roundToGrid( wxPoint( bbox.GetRight(), bbox.GetBottom() ) );
    }
}

void SCH_TINYCAD_SYMBOL::SetBottomRight( wxPoint aPos, int aUnit, bool aForce )
{
    if( aPos.x > m_bottomRight[aUnit].x || aForce )
        m_bottomRight[aUnit].x = aPos.x;

    if( aPos.y < m_bottomRight[aUnit].y || aForce )
        m_bottomRight[aUnit].y = aPos.y;
}

void SCH_TINYCAD_SYMBOL::FixBottomRight()
{
    if( m_bottomRight.find( 0 ) != m_bottomRight.end() )
    {
        for( std::pair<int, wxPoint> el : m_bottomRight )
        {
            SetBottomRight( m_bottomRight[0], el.first );
            SetTopLeft( m_topLeft[0], el.first );
        }
    }
}

void SCH_TINYCAD_SYMBOL::AlignSymbol( ALIGNMENT aAlignment )
{
    FixBottomRight();

    LIB_SYMBOL* symbol = m_symbol;
    wxPoint     offset;
    bool        isInterchangable = !symbol->UnitsLocked();
    if( isInterchangable )
    {
        offset = SCH_TINYCAD_SYMBOL_PARSER::GetAlignmentOffset( symbol, 0, aAlignment );
    }

    for( LIB_SYMBOL_UNITS unit : symbol->GetUnitDrawItems() )
    {
        if( !isInterchangable )
        {
            offset = SCH_TINYCAD_SYMBOL_PARSER::GetAlignmentOffset( symbol, unit.m_unit,
                                                                    aAlignment );
        }

        SetBottomRight( GetBottomRight( unit.m_unit ) + offset, unit.m_unit, true );
        SetTopLeft( GetTopLeft( unit.m_unit ) + offset, unit.m_unit, true );
        if(REF_POINTExists(unit.m_unit))
        {
            SetREF_POINT(GetREF_POINT(unit.m_unit) + offset, unit.m_unit);
        }
        for( LIB_ITEM* item : unit.m_items )
        {
            item->Offset( wxPoint( offset ) );
        }
    }

    FixBottomRight();
}

SCH_TINYCAD_SYMBOL_COLLECTION::SCH_TINYCAD_SYMBOL_COLLECTION()
{
}

SCH_TINYCAD_SYMBOL_COLLECTION::~SCH_TINYCAD_SYMBOL_COLLECTION()
{
}

bool SCH_TINYCAD_SYMBOL_COLLECTION::SymbolExists( wxString aName )
{
    return m_symbolNames.find( aName ) != m_symbolNames.end();
}

void SCH_TINYCAD_SYMBOL_COLLECTION::AddIdToSymbol( wxString aSheetFileName, int aId,
                                                   wxString aName )
{
    m_symbolName_from_id[std::make_pair( aSheetFileName, aId )] = aName;
}

void SCH_TINYCAD_SYMBOL_COLLECTION::AddSymbol( SCH_TINYCAD_SYMBOL* aSymbol )
{
    wxString symbolName = aSymbol->m_symbol->GetName();
    wxString sheetName = aSymbol->m_sheetFileName;
    int      id = aSymbol->m_id;
    if( !SymbolExists( symbolName ) )
    {
        // add the symbol
        m_symbols_by_name[symbolName] = aSymbol;
    }
    m_symbolName_from_id[std::make_pair( sheetName, id )] = symbolName;
    m_symbolNames.insert( aSymbol->m_symbol->GetName() );
}

void SCH_TINYCAD_SYMBOL_COLLECTION::AddSymbol( LIB_SYMBOL* aSymbol, wxString aSheetFileName,
                                               int aId )
{
    AddSymbol( new SCH_TINYCAD_SYMBOL( aSymbol, aSheetFileName, aId ) );
}

SCH_TINYCAD_SYMBOL* SCH_TINYCAD_SYMBOL_COLLECTION::GetSymbol( wxString aSymbolName )
{
    return m_symbols_by_name[aSymbolName];
}

SCH_TINYCAD_SYMBOL* SCH_TINYCAD_SYMBOL_COLLECTION::GetSymbol( wxString aSheetFileName, int aId )
{
    return m_symbols_by_name[m_symbolName_from_id[std::make_pair( aSheetFileName, aId )]];
}

SCH_TINYCAD_PLUGIN::SCH_TINYCAD_PLUGIN()
{
    m_rootSheet = nullptr;
    m_currentSheet = nullptr;
    m_schematic = nullptr;
    m_cache = nullptr;
    m_symbols = new SCH_TINYCAD_SYMBOL_COLLECTION();

    m_pi.set( SCH_IO_MGR::FindPlugin( SCH_IO_MGR::SCH_KICAD ) );
    m_reporter = &WXLOG_REPORTER::GetInstance();
}

class SCH_TINYCAD_PLUGIN_CACHE
{
    wxString   m_fileName;
    wxFileName m_libFileName;

public:
    SCH_TINYCAD_PLUGIN_CACHE( const wxString& aLibraryPath );
    ~SCH_TINYCAD_PLUGIN_CACHE();

    void Save();

    void Load();

    void AddSymbol( LIB_SYMBOL* aSymbol );

    LIB_SYMBOL_MAP m_symbols;
};

SCH_TINYCAD_PLUGIN_CACHE::SCH_TINYCAD_PLUGIN_CACHE( const wxString& aFullPathAndFileName ) :
        m_fileName( aFullPathAndFileName ), m_libFileName( aFullPathAndFileName )
{
    Load();
}

SCH_TINYCAD_PLUGIN_CACHE::~SCH_TINYCAD_PLUGIN_CACHE()
{
    for( auto& symbol : m_symbols )
        delete symbol.second;

    m_symbols.clear();
}

void SCH_TINYCAD_PLUGIN_CACHE::AddSymbol( LIB_SYMBOL* aSymbol )
{
    wxString name = aSymbol->GetName();

    m_symbols[name] = aSymbol;
}

void SCH_TINYCAD_PLUGIN_CACHE::Load()
{
    wxXmlDocument      xmlDocument;
    wxFFileInputStream stream( m_libFileName.GetFullPath() );

    if( !stream.IsOk() || !xmlDocument.Load( stream ) )
    {
        THROW_IO_ERROR( wxString::Format( _( "Unable to read file \"%s\"" ),
                                          m_libFileName.GetFullPath() ) );
    }

    wxXmlNode*      rootNode = xmlDocument.GetRoot();
    NODE_VECTOR_MAP symbolNodes = MapVectorChildren( rootNode );
    for( wxXmlNode* symbol : symbolNodes["SYMBOL"] )
    {
        NODE_VECTOR_MAP symbolChildren = MapVectorChildren( symbol );

        LIB_SYMBOL* parentSymbol = nullptr;

        for( wxXmlNode* detailsNode : symbolChildren["DETAILS"] )
        {
            NODE_VECTOR_MAP detailsChildren = MapVectorChildren( detailsNode );
            //LIB_SYMBOL* symbolPart = new LIB_SYMBOL("placeholder symbol name");
            SCH_TINYCAD_SYMBOL* symbolPart = new SCH_TINYCAD_SYMBOL();
            SCH_TINYCAD_SYMBOL_PARSER::LoadSymbol( symbol, symbolPart, nullptr, nullptr, true,
                                                   detailsNode );

            if( parentSymbol )
            {
                symbolPart->m_symbol->SetParent( parentSymbol );
            }
            else
            {
                parentSymbol = symbolPart->m_symbol;
            }

            AddSymbol( symbolPart->m_symbol );
        }
    }
}

SCH_TINYCAD_PLUGIN::~SCH_TINYCAD_PLUGIN()
{
    delete m_cache;
}

const wxString SCH_TINYCAD_PLUGIN::GetName() const
{
    return "TINYCAD";
}

const wxString SCH_TINYCAD_PLUGIN::GetFileExtension() const
{
    return "dsn";
}

const wxString SCH_TINYCAD_PLUGIN::GetLibraryFileExtension() const
{
    return "xml";
}

int SCH_TINYCAD_PLUGIN::GetModifyHash() const
{
    return 0;
}

void SCH_TINYCAD_PLUGIN::EnumerateSymbolLib( wxArrayString&    aSymbolNameList,
                                             const wxString&   aLibraryPath,
                                             const PROPERTIES* aProperties )
{
    std::cout << "EnumerateSymbolLib arraystring" << std::endl;
    cacheLib( aLibraryPath, aProperties );

    const LIB_SYMBOL_MAP& symbols = m_cache->m_symbols;

    for( LIB_SYMBOL_MAP::const_iterator it = symbols.begin(); it != symbols.end(); ++it )
    {
        std::cout << it->first << std::endl;
        aSymbolNameList.Add( it->first );
    }
}

void SCH_TINYCAD_PLUGIN::EnumerateSymbolLib( std::vector<LIB_SYMBOL*>& aSymbolList,
                                             const wxString&           aLibraryPath,
                                             const PROPERTIES*         aProperties )
{
    std::cout << "EnumerateSymbolLib vector" << std::endl;
    cacheLib( aLibraryPath, aProperties );

    const LIB_SYMBOL_MAP& symbols = m_cache->m_symbols;

    for( LIB_SYMBOL_MAP::const_iterator it = symbols.begin(); it != symbols.end(); ++it )
    {
        aSymbolList.push_back( it->second );
    }

    std::cout << "finished enumerating symbol lib vector" << std::endl;
}

LIB_SYMBOL* SCH_TINYCAD_PLUGIN::LoadSymbol( const wxString&   aLibraryPath,
                                            const wxString&   aSymbolName,
                                            const PROPERTIES* aProperties )
{
    //LIB_SYMBOL_MAP::const_iterator it = m_cache->m_symbols.find( aSymbolName );

    //if( it == m_cache->m_symbols.end() )
    //return nullptr;

    //return it->second;
    cacheLib( aLibraryPath, aProperties );

    LIB_SYMBOL_MAP::const_iterator it = m_cache->m_symbols.find( aSymbolName );

    if( it == m_cache->m_symbols.end() )
        return nullptr;

    return it->second;
}

void SCH_TINYCAD_PLUGIN::SaveSymbol( const wxString& aLibraryPath, const LIB_SYMBOL* aSymbol,
                                     const PROPERTIES* aProperties )
{
    std::cout << "SaveSymbol" << std::endl;
}

void SCH_TINYCAD_PLUGIN::CreateSymbolLib( const wxString&   aLibraryPath,
                                          const PROPERTIES* aProperties )
{
    std::cout << "CreateSymbolLib" << std::endl;
}

bool SCH_TINYCAD_PLUGIN::DeleteSymbolLib( const wxString&   aLibraryPath,
                                          const PROPERTIES* aProperties )
{
    std::cout << "DeleteSymbolLib" << std::endl;
    return true;
}

void SCH_TINYCAD_PLUGIN::SaveLibrary( const wxString& aLibraryPath, const PROPERTIES* aProperties )
{
    std::cout << "SaveLibrary" << std::endl;
}

bool SCH_TINYCAD_PLUGIN::IsSymbolLibWritable( const wxString& aLibraryPath )
{
    return true;
}

void SCH_TINYCAD_PLUGIN::cacheLib( const wxString& aLibraryFileName, const PROPERTIES* aProperties )
{
    if( !m_cache )
    {
        m_cache = new SCH_TINYCAD_PLUGIN_CACHE( aLibraryFileName );
    }
}

SCH_SHEET* SCH_TINYCAD_PLUGIN::Load( const wxString& aFileName, SCHEMATIC* aSchematic,
                                     SCH_SHEET* aAppendToMe, const PROPERTIES* aProperties )
{
    wxASSERT( !aFileName || aSchematic != nullptr );
    LOCALE_IO toggle;

    m_filename = aFileName;
    m_schematic = aSchematic;

    wxXmlDocument      xmlDocument;
    wxFFileInputStream stream( m_filename.GetFullPath() );

    if( !stream.IsOk() || !xmlDocument.Load( stream ) )
    {
        THROW_IO_ERROR(
                wxString::Format( _( "Unable to read file \"%s\"" ), m_filename.GetFullPath() ) );
    }

    // Delete on exception, if I own m_rootSheet, according to aAppendToMe
    unique_ptr<SCH_SHEET> deleter( aAppendToMe ? nullptr : m_rootSheet );

    m_libName = LIB_ID::FixIllegalChars( m_filename.GetName(), true );

    wxFileName libFileName( m_schematic->Prj().GetProjectPath(), m_libName,
                            KiCadSymbolLibFileExtension );

    m_libraryFileName = libFileName;

    m_properties = std::make_unique<PROPERTIES>();

    SYMBOL_LIB_TABLE* libTable = m_schematic->Prj().SchSymbolLibTable();
    wxCHECK_MSG( libTable, NULL, "Could not load symbol lib table." );

    //create new symbol library
    m_pi->CreateSymbolLib( libFileName.GetFullPath() );
    wxString libTableUri = "${KIPRJMOD}/" + libFileName.GetFullName();

    // add & save to project symbol library table
    libTable->InsertRow( new SYMBOL_LIB_TABLE_ROW( m_libName, libTableUri, wxString( "KiCad" ) ) );

    wxFileName fn( m_schematic->Prj().GetProjectPath(),
                   SYMBOL_LIB_TABLE::GetSymbolLibTableFileName() );

    {
        FILE_OUTPUTFORMATTER formatter( fn.GetFullPath() );
        libTable->Format( &formatter, 0 );
    }

    // reload symbol library table
    m_schematic->Prj().SetElem( PROJECT::ELEM_SYMBOL_LIB_TABLE, NULL );
    m_schematic->Prj().SchSymbolLibTable();

    wxXmlNode* currentNode = xmlDocument.GetRoot();
    //m_version = currentNode->GetAttribute("NAME");

    m_pi->SaveLibrary( libFileName.GetFullPath() );

    if( aAppendToMe )
    {
        wxCHECK_MSG( aSchematic->IsValid(), nullptr, "Can't append to a schematic with no root!" );
        m_rootSheet = &aSchematic->Root();
    }
    else
    {
        m_rootSheet = new SCH_SHEET( m_schematic );
    }

    SCH_SCREEN* rootScreen;
    if( !m_rootSheet->GetScreen() )
    {
        rootScreen = new SCH_SCREEN( m_schematic );
        m_rootSheet->SetScreen( rootScreen );
    }
    else
    {
        rootScreen = m_rootSheet->GetScreen();
    }

    m_rootPath.push_back( m_rootSheet );
    m_rootSheet->AddInstance( m_rootPath.Path() );
    //m_rootSheet->SetPageNumber( m_rootPath, wxT( "0" ) );
    rootScreen->SetVirtualPageNumber( 0 );

    NODE_VECTOR_MAP children = MapVectorChildren( currentNode );

    bool singleSheet = children["TinyCAD"].size() == 1;

    m_currentSheet = m_rootSheet;
    if( singleSheet )
    {
        loadSheet( children["TinyCAD"][0], SHEET_TYPE::ROOT );
    }
    else
    {
        setSheetFileName( SHEET_TYPE::ROOT );
        wxString sheetDir = m_schematic->Prj().GetProjectPath() + "/sub-pages";
        if( !wxDirExists( sheetDir ) )
        {
            wxMkdir( sheetDir );
        }

        int x = 0;
        int y = 0;
        int pagenum = 1;
        for( wxXmlNode* sheetNode : children["TinyCAD"] )
        {
            SCH_SHEET* sheet = new SCH_SHEET( m_schematic );
            sheet->SetFlags( IS_NEW );

            m_rootPath.push_back( sheet );
            sheet->AddInstance( m_rootPath.Path() );
            std::cout << "Current page number: " << pagenum << std::endl;

            sheet->SetPosition(
                    wxPoint( convertToEeschemaUnits( 6 ), convertToEeschemaUnits( y * 10 + 10 ) ) );
            SCH_SCREEN* screen = new SCH_SCREEN( m_schematic );
            sheet->SetScreen( screen );
            screen->SetVirtualPageNumber( pagenum );
            sheet->AutoAutoplaceFields( screen );
            m_currentSheet = sheet;

            loadSheet( sheetNode, SHEET_TYPE::SUB_PAGE );
            saveSheet();

            m_rootSheet->GetScreen()->Append( m_currentSheet );

            pagenum++;
            y++;
        }
    }

    m_currentSheet = m_rootSheet;
    saveSheet();

    std::cout << "All sheets loaded" << std::endl;

    //EDA_RECT sheetBbox = getSheetBbox( m_rootSheet );
    //m_rootSheet->LocatePathOfScreen( m_rootSheet->GetScreen(), sheetBbox.GetBottom() );


    // empty.kicad_wks is created when Load is finished?
    // DrawingSheetFileExtension

    return m_rootSheet;
}

void SCH_TINYCAD_PLUGIN::setSheetFileName( SHEET_TYPE aSheetType )
{
    switch( aSheetType )
    {
    case SHEET_TYPE::ROOT:
    {
        wxFileName fn( m_filename );
        fn.SetExt( KiCadSchematicFileExtension );

        m_currentSheet->GetScreen()->SetFileName( fn.GetFullPath() );
        m_currentSheet->SetFileName( fn.GetFullPath() );
        break;
    }
    case SHEET_TYPE::SUB_PAGE:
    {
        wxString sheetFileName = m_currentSheet->GetName();
        sheetFileName.Replace( " ", "_" );

        std::string dir = "sub-pages/";
        std::string filename = dir + sheetFileName.ToStdString() + std::string( "." )
                               + KiCadSchematicFileExtension;
        wxFileName fn( m_schematic->Prj().GetProjectPath() + filename );

        m_currentSheet->GetScreen()->SetFileName( fn.GetFullPath() );
        m_currentSheet->SetFileName( filename );
        break;
    }
    case SHEET_TYPE::HIERARCHICAL:
    {
        wxString   filename = m_currentSheet->GetFileName();
        wxFileName fn( m_schematic->Prj().GetProjectPath() + filename );

        m_currentSheet->GetScreen()->SetFileName( fn.GetFullPath() );
        m_currentSheet->SetFileName( filename );
        break;
    }
    }
}

//void SCH_TINYCAD_PLUGIN::saveSheet(bool aRootSheet, bool aHierarchicalSheet, wxString aAlternatePath)
void SCH_TINYCAD_PLUGIN::saveSheet()
{
    m_pi->Save( m_currentSheet->GetScreen()->GetFileName(), m_currentSheet, m_schematic );
}

void SCH_TINYCAD_PLUGIN::loadSheet( wxXmlNode* aSheetNode, SHEET_TYPE aSheetType,
                                    std::set<wxString> aHierarchicalPins )
{
    SYMBOL_LIB_TABLE& schLibTable = *m_schematic->Prj().SchSymbolLibTable();

    NODE_MAP        sheetChildren = MapChildren( aSheetNode );
    NODE_VECTOR_MAP sheetChildrenVector = MapVectorChildren( aSheetNode );
    SCH_FIELD&      sheetNameField = m_currentSheet->GetFields()[SHEETNAME];
    SCH_FIELD&      sheetFileNameField = m_currentSheet->GetFields()[SHEETFILENAME];

    wxString sheetName = sheetChildren["NAME"]->GetNodeContent();
    sheetNameField.SetText( EscapeString( sheetName, ESCAPE_CONTEXT::CTX_NETNAME ) );

    std::cout << "Sheet name: " << sheetChildren["NAME"]->GetNodeContent() << std::endl;

    setSheetFileName( aSheetType );

    std::map<int, textFont*>*  textSizeMap = new std::map<int, textFont*>;
    std::map<int, lineStyle*>* lineStyleMap = new std::map<int, lineStyle*>;

    for( wxXmlNode* fontDefNode : sheetChildrenVector["FONT"] )
    {
        ( *textSizeMap )[wxAtoi( fontDefNode->GetAttribute( "id" ) )] = SCH_TINYCAD_SYMBOL_PARSER::LoadTextFont(fontDefNode);
    }

    for( wxXmlNode* styleDefNode : sheetChildrenVector["STYLE"] )
    {
        NODE_VECTOR_MAP styleChildrenList = MapVectorChildren( styleDefNode );
        lineStyle*      ls = new lineStyle();

        int thickness = wxAtoi( styleChildrenList["THICKNESS"][0]->GetNodeContent() );
        ls->width = thickness * 1270;

        switch( wxAtoi( styleChildrenList["STYLE"][0]->GetNodeContent() ) )
        {
        default:
        case 0: ls->style = PLOT_DASH_TYPE::SOLID; break;
        case 1: ls->style = PLOT_DASH_TYPE::DASH; break;
        case 2: ls->style = PLOT_DASH_TYPE::DOT; break;
        case 3: ls->style = PLOT_DASH_TYPE::DASHDOT; break;
        }

        ( *lineStyleMap )[wxAtoi( styleDefNode->GetAttribute( "id" ) )] = ls;
    }

    for( wxXmlNode* symbolDefNode : sheetChildrenVector["SYMBOLDEF"] )
    {
        wxString sheetFileName = m_currentSheet->GetFileName();
        int      id = wxAtoi( symbolDefNode->GetAttribute( "id" ) );
        wxString symbolName = EscapeString( MapChildren( symbolDefNode )["NAME"]->GetNodeContent(),
                                            ESCAPE_CONTEXT::CTX_NETNAME );

        if( !m_symbols->SymbolExists( symbolName ) )
        {
            SCH_TINYCAD_SYMBOL* symbolPart =
                    new SCH_TINYCAD_SYMBOL( new LIB_SYMBOL( symbolName ), sheetFileName, id );

            SCH_TINYCAD_SYMBOL_PARSER::LoadSymbol( symbolDefNode, symbolPart, textSizeMap,
                                                   lineStyleMap );

            LIB_ID libId( m_libName, symbolName );
            symbolPart->m_symbol->SetLibId( libId );
            m_pi->SaveSymbol( m_libraryFileName.GetFullPath(), symbolPart->m_symbol );

            m_symbols->AddSymbol( symbolPart );
        }
        else
        {
            m_symbols->AddIdToSymbol( sheetFileName, id, symbolName );
        }
    }

    loadPageInfo( sheetChildren["DETAILS"] );

    SCH_SCREEN* screen = m_currentSheet->GetScreen();
    bool        missingRefPoint = false;

    SCH_TINYCAD_SYMBOL*      symbol;
    SCH_SYMBOL*              component;
    std::vector<SCH_SYMBOL*> components;
    for( wxXmlNode* symbolNode : sheetChildrenVector["SYMBOL"] )
    {
        wxString        symbolName;
        NODE_VECTOR_MAP symbolChildren = MapVectorChildren( symbolNode );
        for( wxXmlNode* fieldNode : symbolChildren["FIELD"] )
        {
            if( fieldNode->GetAttribute( "description" ).IsSameAs( "Name" ) )
            {
                symbolName = EscapeString( fieldNode->GetAttribute( "value" ),
                                           ESCAPE_CONTEXT::CTX_NETNAME );
            }
        }
        std::cout << "Placing symbol " << symbolName << " in sheet" << std::endl;
        //symbol = m_symbols->GetSymbol(symbolName);
        wxString sheetFileName = m_currentSheet->GetFileName();
        int      symbolId = wxAtoi( symbolNode->GetAttribute( "id" ) );
        symbol = m_symbols->GetSymbol( sheetFileName, symbolId );

        if( !symbol->m_symbol->GetName().IsSameAs( symbolName ) )
        {
            std::cout << "Symbol with id: " << symbolNode->GetAttribute( "id" )
                      << " does not match name" << std::endl;
            std::cout << symbolName << " instead of " << symbol->m_symbol->GetName() << std::endl;
            // create a duplicate symbol if it does not exist in our collection
            if( !m_symbols->SymbolExists( symbolName ) )
            {
                SCH_TINYCAD_SYMBOL* newSymbol = new SCH_TINYCAD_SYMBOL( symbol );
                newSymbol->m_symbol->SetName( symbolName );
                m_symbols->AddSymbol( newSymbol );
                std::cout << "Created duplicate symbol with name: "
                          << newSymbol->m_symbol->GetName() << std::endl;
            }

            symbol = m_symbols->GetSymbol( symbolName );

            std::cout << "Got duplicate symbol: " << symbol->m_symbol->GetName() << std::endl;
        }

        int unit = wxAtoi( symbolNode->GetAttribute( "part" ) ) + 1;

        component = new SCH_SYMBOL( parsePosition( symbolNode->GetAttribute( "pos" ) ),
                                    m_currentSheet );
        //LIB_ID libId = ToKCadLibID(m_currentSheet->GetName(), m_libName, symbol->GetName());
        //LIB_ID libId( m_libName, symbol->m_symbol->GetName());
        component->SetLibId( symbol->m_symbol->GetLibId() );
        component->SetUnit( unit );

        // should be getting lib symbol this way but does not work for some reason?
        //LIB_SYMBOL* libSymbol = schLibTable.LoadSymbol( libId );
        //if (libSymbol == NULL)
        //{
        //}
        //component->SetLibSymbol( new LIB_SYMBOL( *libSymbol ) );
        component->SetLibSymbol( symbol->m_symbol );

        //component->SetPosition(  );
        //component->SetPosition(
        //    component->GetPosition() +
        //    SCH_TINYCAD_SYMBOL_PARSER::GetAlignmentOffset(symbol2, unit, ALIGNMENT::BOTTOMRIGHT)
        //);

        wxPoint  pos = roundToGrid(component->GetPosition());
        EDA_RECT bbox = component->GetBodyBoundingBox();
        // maybe use bbox.offset(-pos)

        wxPoint bottomRight = symbol->GetBottomRight( unit );
        wxPoint topLeft = symbol->GetTopLeft( unit );

        int width = std::abs(
                roundToGrid( symbol->GetTopLeft( unit ).x - symbol->GetBottomRight( unit ).x ) );
        int height = std::abs(
                roundToGrid( symbol->GetTopLeft( unit ).y - symbol->GetBottomRight( unit ).y ) );

        //wxPoint diff = roundToGrid( wxPoint( bottomRight.x, -bottomRight.y ) );
        wxPoint diff = symbol->REF_POINTExists(unit) ? wxPoint(symbol->GetREF_POINT( unit ).x, -symbol->GetREF_POINT( unit ).y) : roundToGrid( wxPoint( bottomRight.x, -bottomRight.y ) );
        wxPoint diff2 = -roundToGrid( wxPoint( topLeft.x, -topLeft.y ) );

        switch( wxAtoi( symbolNode->GetAttribute( "rotate" ) ) )
        {
        default:
        case 0: // Up / Not mirrored
            component->SetPosition( wxPoint( pos.x - diff.x, pos.y - diff.y ) );
            break;
        case 1: // Mirror down / MirrorX
            component->SetOrientation( SYM_MIRROR_X );
            component->SetPosition( wxPoint( pos.x - diff.x, pos.y - diff2.y ) );
            break;
        case 2: // Mirror Right / MirrorX 90 deg
            component->SetOrientation( SYM_ROTATE_CLOCKWISE );
            component->SetOrientation( SYM_MIRROR_Y );
            component->SetPosition( wxPoint( pos.x - diff.y, pos.y - diff.x ) );
            break;
        case 3: // Right / -90 deg
            component->SetOrientation( SYM_ROTATE_CLOCKWISE );
            component->SetPosition( wxPoint( pos.x - diff2.y, pos.y - diff.x ) );
            break;
        case 4: // Mirror / MirrorY
            component->SetOrientation( SYM_MIRROR_Y );
            component->SetPosition( wxPoint( pos.x - diff2.x, pos.y - diff.y ) );
            break;
        case 5: // Down / 180 deg
            component->SetOrientation( SYM_ORIENT_180 );
            component->SetPosition( wxPoint( pos.x - diff2.x, pos.y - diff2.y ) );
            break;
        case 6: // Left / 90 deg
            component->SetOrientation( SYM_ROTATE_COUNTERCLOCKWISE );
            component->SetPosition( wxPoint( pos.x - diff.y, pos.y - diff2.x ) );
            break;
        case 7: // Mirror Left / MirrorX -90 deg
            component->SetOrientation( SYM_ROTATE_COUNTERCLOCKWISE );
            component->SetOrientation( SYM_MIRROR_Y );
            component->SetPosition( wxPoint( pos.x - diff2.y, pos.y - diff2.x ) );
            break;
        }

        loadFieldAttributes( component, symbolChildren["FIELD"] );

        if (!symbol->REF_POINTExists(unit))
        {
            if( !missingRefPoint )
            {
                m_reporter->Report( wxString::Format(
                        _( "In sheet '%s' the following components might be placed incorrectly:" ),
                        sheetName )
                        );
            }
            missingRefPoint = true;

            m_reporter->Report( wxString::Format( 
                                                _( "%s (%s)"), 
                                                component->GetRef(&m_rootPath, true),
                                                UnescapeString( symbol->m_symbol->GetName() ) )
                                                );
        }

        std::cout << "Symbol placed at: " << component->GetPosition() << std::endl;

        components.push_back( component );

        screen->Append( component );
    }
    std::cout << "Placed all symbols in current sheet" << std::endl;

    for( wxXmlNode* hierarchicalNode : sheetChildrenVector["HIERARCHICAL_SYMBOL"] )
    {
        wxString currentPath = m_filename.GetPath();
        wxString fileName = hierarchicalNode->GetAttribute( "file" );
        if( fileName.StartsWith( ".\\" ) )
            fileName = "/" + fileName.AfterFirst( '\\' );
        else if( fileName.StartsWith( "." ) )
            fileName = fileName.AfterFirst( '.' );

        wxFileName         hierarchicalFileName( currentPath + fileName );
        wxXmlDocument      hierarchicalFile;
        wxFFileInputStream stream( hierarchicalFileName.GetFullPath() );

        if( !stream.IsOk() || !hierarchicalFile.Load( stream ) )
        {
            THROW_IO_ERROR( wxString::Format( _( "Unable to read file \"%s\"" ),
                                              hierarchicalFileName.GetFullPath() ) );
        }

        wxXmlNode*      rootNode = hierarchicalFile.GetRoot();
        NODE_VECTOR_MAP rootChildren = MapVectorChildren( rootNode );


        SCH_SHEET* sheet = new SCH_SHEET( m_schematic );
        sheet->SetFlags( IS_NEW );
        m_rootPath.push_back( sheet );
        sheet->AddInstance( m_rootPath.Path() );
        SCH_SCREEN* screen = new SCH_SCREEN( m_schematic );
        sheet->SetScreen( screen );
        sheet->SetFileName( hierarchicalFileName.GetName() + "." + KiCadSchematicFileExtension );

        NODE_VECTOR_MAP hierarchicalChildren =
                MapVectorChildren( rootChildren["HierarchicalSymbol"][0] );

        wxPoint pos1 = parsePosition( hierarchicalChildren["RECTANGLE"][0]->GetAttribute( "a" ) );
        wxPoint pos2 = parsePosition( hierarchicalChildren["RECTANGLE"][0]->GetAttribute( "b" ) );
        wxPoint pos = parsePosition( hierarchicalNode->GetAttribute( "pos" ) );

        std::set<wxString> hierarchicalPins;
        // increase the size of the sheet symbol depending on the length of pins
        int loffset = 0, uoffset = 0, roffset = 0, doffset = 0;
        for( wxXmlNode* pinNode : hierarchicalChildren["PIN"] )
        {
            int pinLength =
                    convertToEeschemaUnits( wxAtoi( pinNode->GetAttribute( "length" ) ) ) / 5;
            int dir = wxAtoi( pinNode->GetAttribute( "direction" ) );
            switch( dir )
            {
            default:
                wxASSERT_MSG( false, wxString::Format( "Unhandled orientation" ) );
                KI_FALLTHROUGH;
            case 0:
                if( uoffset < pinLength )
                    uoffset = pinLength;
                break;
            case 1:
                if( doffset < pinLength )
                    doffset = pinLength;
                break;
            case 2:
                if( loffset < pinLength )
                    loffset = pinLength;
                break;
            case 3:
                if( roffset < pinLength )
                    roffset = pinLength;
                break;
            }
        }

        wxSize size = wxSize( pos2.x - pos1.x, pos2.y - pos1.y );
        size = wxSize( size.GetWidth() + loffset + roffset, size.GetHeight() + uoffset + doffset );
        sheet->SetSize( size );
        wxPoint topLeft = wxPoint( pos.x - size.GetWidth(), pos.y - size.GetHeight() );
        sheet->SetPosition( topLeft );

        // load all the pins
        for( wxXmlNode* pinNode : hierarchicalChildren["PIN"] )
        {
            wxString pinText = pinNode->GetNodeContent();
            hierarchicalPins.insert( pinText );
            wxPoint        pinPos = parsePosition( pinNode->GetAttribute( "pos" ) );
            SCH_SHEET_PIN* pin = new SCH_SHEET_PIN( sheet );
            pin->SetText( pinText );

            int dir = wxAtoi( pinNode->GetAttribute( "direction" ) );
            int pinLength =
                    convertToEeschemaUnits( wxAtoi( pinNode->GetAttribute( "length" ) ) ) / 5;
            wxPoint newPos = wxPoint( pinPos.x - pos2.x + pos.x, pinPos.y - pos2.y + pos.y );

            pin->SetPosition( wxPoint( pinPos.x - loffset - pos2.x + pos.x,
                                       pinPos.y - uoffset - pos2.y + pos.y ) );
            sheet->AddPin( pin );
        }

        SCH_SHEET* oldCurrentSheet = m_currentSheet;

        m_currentSheet = sheet;
        loadSheet( rootChildren["TinyCAD"][0], SHEET_TYPE::HIERARCHICAL, hierarchicalPins );

        NODE_VECTOR_MAP hierarchicalNodeChildren = MapVectorChildren( hierarchicalNode );
        for( wxXmlNode* fieldNode : hierarchicalNodeChildren["FIELD"] )
        {
            if( fieldNode->GetAttribute( "description" ).IsSameAs( "Ref" ) )
            {
                sheet->GetFields()[SHEETNAME].SetText( fieldNode->GetAttribute( "value" ) );
            }
        }

        for( SCH_ITEM* schItem :
             m_currentSheet->GetScreen()->Items().OfType( KICAD_T::SCH_SYMBOL_T ) )
        {
            SCH_SYMBOL* schSymbol = dynamic_cast<SCH_SYMBOL*>( schItem );
            SCH_FIELD*  refField = schSymbol->GetField( MANDATORY_FIELD_T::REFERENCE_FIELD );
            // Append sheetname to symbol references, but not to power symbols
            if( schSymbol->IsInNetlist() )
            {
                refField->SetText( refField->GetText() + "_" + m_currentSheet->GetName() );
            }
        }

        // update sheet file name to relative of parent sheet
        wxFileName hierFn( sheet->GetScreen()->GetFileName() );
        wxFileName parentFn( oldCurrentSheet->GetScreen()->GetFileName() );
        hierFn.MakeRelativeTo( parentFn.GetPath() );
        sheet->SetFileName( hierFn.GetFullPath() );

        saveSheet();

        m_currentSheet = oldCurrentSheet;
        sheet->AutoAutoplaceFields( m_currentSheet->GetScreen() );
        m_currentSheet->GetScreen()->Append( sheet );
    }


    SCH_LINE* wire = nullptr;
    for( wxXmlNode* wireNode : sheetChildrenVector["WIRE"] )
    {
        wire = loadWire( wireNode );
        screen->Append( wire );
    }

    std::vector<SCH_LINE*> polygon;
    for( wxXmlNode* polygonNode : sheetChildrenVector["POLYGON"] )
    {
        polygon = loadPolygoneLines( polygonNode, lineStyleMap );
        for( SCH_LINE* line : polygon )
        {
            screen->Append( line );
        }
    }

    std::vector<SCH_LINE*> rectangle;
    for( wxXmlNode* rectangleNode : sheetChildrenVector["RECTANGLE"] )
    {
        rectangle = loadRectangleLines( rectangleNode, lineStyleMap );
        for( SCH_LINE* line : rectangle )
        {
            screen->Append( line );
        }
    }

    SCH_LINE* bus = nullptr;
    for( wxXmlNode* busNode : sheetChildrenVector["BUS"] )
    {
        bus = loadBus( busNode );
        screen->Append( bus );
    }

    SCH_BUS_WIRE_ENTRY* busEntry = nullptr;
    for( wxXmlNode* busEntryNode : sheetChildrenVector["BUSSLASH"] )
    {
        busEntry = loadBusEntry( busEntryNode );
        screen->Append( busEntry );
    }

    SCH_TEXT* busLabel = nullptr;
    for( wxXmlNode* busLabelNode : sheetChildrenVector["BUSNAME"] )
    {
        busLabel = loadBusLabel( busLabelNode );
        screen->Append( busLabel );
    }

    SCH_TEXT* label = nullptr;
    for( wxXmlNode* labelNode : sheetChildrenVector["LABEL"] )
    {
        label = loadLabel( labelNode, aHierarchicalPins );
        screen->Append( label );
    }

    SCH_JUNCTION* junction = nullptr;
    for( wxXmlNode* junctionNode : sheetChildrenVector["JUNCTION"] )
    {
        junction = loadJunction( junctionNode );
        screen->Append( junction );
    }

    SCH_SYMBOL* powerComponent;
    for( wxXmlNode* powerNode : sheetChildrenVector["POWER"] )
    {
        powerComponent = loadPower( powerNode );

        if( powerComponent->GetLibId().empty() )
        {
            // If no power symbol with this name was found, create a general purpose label instead
            SCH_TEXT* powerLabel = nullptr;
            powerLabel = loadPowerLabel( powerNode );
            screen->Append( powerLabel );
        }
        else
        {
            powerComponent->SetRef( &m_rootPath, "#PWR?" );
            components.push_back( powerComponent );
            screen->Append( powerComponent );
        }
    }

    SCH_NO_CONNECT* NC = nullptr;
    for( wxXmlNode* NCNode : sheetChildrenVector["NOCONNECT"] )
    {
        NC = loadNC( NCNode );
        screen->Append( NC );
    }

    SCH_TEXT* text = nullptr;
    for( wxXmlNode* textNode : sheetChildrenVector["TEXT"] )
    {
        text = loadText( textNode, textSizeMap );
        screen->Append( text );
    }

    for( SCH_SYMBOL* comp : components )
    {
        comp->AutoplaceFields( screen, false );
    }
}

//https://gitlab.com/kicad/code/kicad/-/blob/master/eeschema/sch_plugins/legacy/sch_legacy_plugin.cpp#L905
void SCH_TINYCAD_PLUGIN::loadPageInfo( wxXmlNode* aDetailsNode )
{
    NODE_MAP  detailsChildren = MapChildren( aDetailsNode );
    PAGE_INFO pageInfo;

    pageInfo.SetCustomHeightMils( wxAtoi( detailsChildren["Size"]->GetAttribute( "height" ) )
                                  * 10 );
    pageInfo.SetCustomWidthMils( wxAtoi( detailsChildren["Size"]->GetAttribute( "width" ) ) * 10 );
    pageInfo.SetType( "User" );

    m_currentSheet->GetScreen()->SetPageSettings( pageInfo );

    //m_sheetOffset = { 0, pageInfo.GetHeightIU() };

    TITLE_BLOCK tb;
    tb.SetTitle( detailsChildren["TITLE"]->GetNodeContent() );
    tb.SetDate( detailsChildren["DATE"]->GetNodeContent() );
    tb.SetCompany( detailsChildren["ORGANISATION"]->GetNodeContent() );
    tb.SetRevision( detailsChildren["REVISION"]->GetNodeContent() );
    tb.SetComment( 0, detailsChildren["AUTHOR"]->GetNodeContent() );

    m_currentSheet->GetScreen()->SetTitleBlock( tb );

    // m_schematic->Prj().GetTextVars()[ NAME ] = elem.text
}

SCH_TEXT* SCH_TINYCAD_PLUGIN::loadText( wxXmlNode*                aTextNode,
                                        std::map<int, textFont*>* aTextFontMap )
{
    SCH_TEXT* text = new SCH_TEXT( parsePosition( aTextNode->GetAttribute( "pos" ) ),
                                   aTextNode->GetNodeContent() );

    if( aTextNode->GetAttribute( "direction" ) == '3' )
    {
        text->SetLabelSpinStyle( LABEL_SPIN_STYLE::RIGHT );
    }
    else
    {
        text->SetLabelSpinStyle( LABEL_SPIN_STYLE::UP );
    }

    int id = wxAtoi( aTextNode->GetAttribute( "font" ) );
    int size = ( *aTextFontMap )[id]->size;
    text->SetTextSize( wxSize( size, size ) );
    text->SetBold( ( *aTextFontMap )[id]->bold );
    text->SetItalic( ( *aTextFontMap )[id]->italic );

    return text;
}

SCH_NO_CONNECT* SCH_TINYCAD_PLUGIN::loadNC( wxXmlNode* aNCNode )
{
    SCH_NO_CONNECT* NC = new SCH_NO_CONNECT( parsePosition( aNCNode->GetAttribute( "pos" ) ) );
    return NC;
}

SCH_SYMBOL* SCH_TINYCAD_PLUGIN::loadPower( wxXmlNode* aPowerNode )
{
    // search in all power libraries

    //SYMBOL_LIBRARY_MANAGER* libMgr;
    //SYMBOL_LIBS* symbolLibs = m_schematic->Prj().SchLibs();
    //symbolLibs->LoadAllLibraries(&(m_schematic->Prj()));
    //wxArrayString libraries = symbolLibs->GetLibraryNames();
    //for (wxString library : libraries)
    //{

    //}
    //SYMBOL_LIB* powerLib = symbolLibs->FindLibrary("power");
    //wxASSERT_MSG(powerLib != NULL, "Could not find power library");

    SYMBOL_LIB_TABLE* schLibTable = m_schematic->Prj().SchSymbolLibTable();

    wxString powerName = aPowerNode->GetNodeContent();

    LIB_SYMBOL* powerSymbol = NULL;

    // Let's see if we've looked for this power symbol already
    if( m_powerSymbolsList.count( powerName ) )
    {
        powerSymbol = m_powerSymbolsList[powerName];
    }
    else
    {
        // We havn't yet seen this power symbol, let's check in the libraries
        const std::vector<wxString> libNicknames = schLibTable->GetLogicalLibs();

        for( wxString libNick : libNicknames )
        {
            powerSymbol = schLibTable->LoadSymbol( libNick, powerName );
            if( powerSymbol != NULL )
            {
                if( powerSymbol->IsPower() )
                {
                    // We have found the power symbol we were looking for. Stop looking
                    break;
                }
                else
                {
                    // The symbol we found wasn't a power symbol. Keep looking.
                    powerSymbol = NULL;
                }
            }
        }

        // Add the power symbol to our list, even if it's NULL.
        m_powerSymbolsList[powerName] = powerSymbol;

        if( powerSymbol == NULL )
        {
            m_reporter->Report( wxString::Format(
                            _( "Power symbol '%s' not found in any library, replaced by a label." ),
                            powerName ),
                            RPT_SEVERITY_INFO );
        }
    }

    SCH_SYMBOL* component =
            new SCH_SYMBOL( parsePosition( aPowerNode->GetAttribute( "pos" ) ), m_currentSheet );
    if( powerSymbol == NULL )
    {
        // try with different search term
        std::cout << "Could not find power symbol matching: " << aPowerNode->GetNodeContent()
                  << std::endl;
    }
    else
    {
        component->SetLibId( powerSymbol->GetLibId() );
        component->SetLibSymbol( powerSymbol );
    }
    component->GetField( MANDATORY_FIELD_T::VALUE_FIELD )->SetText( powerName );
    component->GetField( MANDATORY_FIELD_T::REFERENCE_FIELD )->SetText( "#PWR?" );

    int dir = wxAtoi( aPowerNode->GetAttribute( "direction" ) );
    if( powerName.Matches( "*GND*" ) || powerName.Matches( "*Earth*" ) )
    {
        // GND power symbols are drawn upside down compared to other power symbols
        switch( dir )
        {
        default:
        case 0:
            //up
            component->SetOrientation( SYM_ORIENT_180 );
            break;
        case 1:
            //down
            break;
        case 2:
            //left
            component->SetOrientation( SYM_ORIENT_270 );
            break;
        case 3:
            //right
            component->SetOrientation( SYM_ORIENT_90 );
            break;
        }
    }
    else
    {
        switch( dir )
        {
        default:
        case 0:
            //up
            break;
        case 1:
            //down
            component->SetOrientation( SYM_ORIENT_180 );
            break;
        case 2:
            //left
            component->SetOrientation( SYM_ORIENT_90 );
            break;
        case 3:
            //right
            component->SetOrientation( SYM_ORIENT_270 );
            break;
        }
    }

    component->GetField( MANDATORY_FIELD_T::VALUE_FIELD )->SetVisible( true );

    return component;
}

SCH_LINE* SCH_TINYCAD_PLUGIN::loadWire( wxXmlNode* aWireNode )
{
    SCH_LINE* wire = new SCH_LINE();
    wire->SetStartPoint( parsePosition( aWireNode->GetAttribute( "a" ) ) );
    wire->SetEndPoint( parsePosition( aWireNode->GetAttribute( "b" ) ) );
    wire->SetLayer( LAYER_WIRE );
    return wire;
}

std::vector<SCH_LINE*>
SCH_TINYCAD_PLUGIN::loadPolygoneLines( wxXmlNode*                 aPolygonNode,
                                       std::map<int, lineStyle*>* aLineStyleMap )
{
    wxPoint pgPos = parsePosition( aPolygonNode->GetAttribute( "pos" ) );

    int            id = wxAtoi( aPolygonNode->GetAttribute( "style" ) );
    int            size = 1270;
    PLOT_DASH_TYPE style = PLOT_DASH_TYPE::SOLID;

    if( ( *aLineStyleMap ).count( id ) )
    {
        size = ( *aLineStyleMap )[id]->width;
        style = ( *aLineStyleMap )[id]->style;
    }

    wxXmlNode* vertex = aPolygonNode->GetChildren();
    wxPoint    previousPos = parsePosition( vertex->GetAttribute( "pos" ) ) + pgPos;
    vertex = vertex->GetNext();

    std::vector<SCH_LINE*> pgLines;
    while( vertex )
    {
        SCH_LINE* line = new SCH_LINE();
        line->SetStartPoint( previousPos );
        wxPoint thisPos = parsePosition( vertex->GetAttribute( "pos" ) ) + pgPos;
        line->SetEndPoint( thisPos );
        line->SetLineStyle( style );
        line->SetLayer( LAYER_NOTES );
        line->SetLineWidth( size );
        pgLines.push_back( line );
        previousPos = thisPos;
        vertex = vertex->GetNext();
    }

    return pgLines;
}

std::vector<SCH_LINE*>
SCH_TINYCAD_PLUGIN::loadRectangleLines( wxXmlNode*                 aRectangleNode,
                                        std::map<int, lineStyle*>* aLineStyleMap )
{
    std::vector<SCH_LINE*> rectangleLines;

    wxPoint corner1 = parsePosition( aRectangleNode->GetAttribute( "a" ) );
    wxPoint corner3 = parsePosition( aRectangleNode->GetAttribute( "b" ) );
    wxPoint corner2 = wxPoint( corner3.x, corner1.y );
    wxPoint corner4 = wxPoint( corner1.x, corner3.y );

    int            id = wxAtoi( aRectangleNode->GetAttribute( "style" ) );
    int            size = 1270;
    PLOT_DASH_TYPE style = PLOT_DASH_TYPE::SOLID;

    if( ( *aLineStyleMap ).count( id ) )
    {
        size = ( *aLineStyleMap )[id]->width;
        style = ( *aLineStyleMap )[id]->style;
    }

    SCH_LINE* line1 = new SCH_LINE();
    line1->SetLineStyle( style );
    line1->SetLayer( LAYER_NOTES );
    line1->SetStartPoint( corner1 );
    line1->SetEndPoint( corner2 );
    line1->SetLineWidth( size );
    rectangleLines.push_back( line1 );

    SCH_LINE* line2 = new SCH_LINE();
    line2->SetLineStyle( style );
    line2->SetLayer( LAYER_NOTES );
    line2->SetStartPoint( corner2 );
    line2->SetEndPoint( corner3 );
    line2->SetLineWidth( size );
    rectangleLines.push_back( line2 );

    SCH_LINE* line3 = new SCH_LINE();
    line3->SetLineStyle( style );
    line3->SetLayer( LAYER_NOTES );
    line3->SetStartPoint( corner3 );
    line3->SetEndPoint( corner4 );
    line3->SetLineWidth( size );
    rectangleLines.push_back( line3 );

    SCH_LINE* line4 = new SCH_LINE();
    line4->SetLineStyle( style );
    line4->SetLayer( LAYER_NOTES );
    line4->SetStartPoint( corner4 );
    line4->SetEndPoint( corner1 );
    line4->SetLineWidth( size );
    rectangleLines.push_back( line4 );

    return rectangleLines;
}

SCH_LINE* SCH_TINYCAD_PLUGIN::loadBus( wxXmlNode* aBusNode )
{
    SCH_LINE* bus = new SCH_LINE();
    bus->SetStartPoint( parsePosition( aBusNode->GetAttribute( "a" ) ) );
    bus->SetEndPoint( parsePosition( aBusNode->GetAttribute( "b" ) ) );
    bus->SetLayer( LAYER_BUS );
    return bus;
}

SCH_BUS_WIRE_ENTRY* SCH_TINYCAD_PLUGIN::loadBusEntry( wxXmlNode* aBusEntryNode )
{
    SCH_BUS_WIRE_ENTRY* busEntry = new SCH_BUS_WIRE_ENTRY();

    // The size of the bus joins (bus entries) is fixed in TinyCad, therefor a fixed
    // constant can be used for size and difference in positon compared to the "pos" attribute.
    const int tcLength = convertToEeschemaUnits( 4 );
    wxPoint   tcPos = parsePosition( aBusEntryNode->GetAttribute( "pos" ) );
    wxPoint   busEntryPos;

    if( wxAtoi( aBusEntryNode->GetAttribute( "direction" ) ) )
    {
        busEntryPos = tcPos - wxPoint( tcLength, tcLength );
        busEntry->SetSize( wxSize( tcLength, tcLength ) );
    }
    else
    {
        busEntryPos = tcPos - wxPoint( tcLength, -tcLength );
        busEntry->SetSize( wxSize( tcLength, -tcLength ) );
    }

    busEntry->SetPosition( busEntryPos );
    busEntry->SetLayer( LAYER_BUS );

    return busEntry;
}


SCH_TEXT* SCH_TINYCAD_PLUGIN::loadBusLabel( wxXmlNode* aBusLabelNode )
{
    SCH_TEXT* busLabel;
    busLabel = new SCH_LABEL();

    if( aBusLabelNode->GetAttribute( "direction" ) == '3' )
    {
        busLabel->SetLabelSpinStyle( LABEL_SPIN_STYLE::RIGHT );
    }
    else
    {
        busLabel->SetLabelSpinStyle( LABEL_SPIN_STYLE::UP );
    }

    busLabel->SetText( aBusLabelNode->GetNodeContent() );
    busLabel->SetPosition( parsePosition( aBusLabelNode->GetAttribute( "pos" ) ) );
    busLabel->SetVisible( true );
    return busLabel;
}

SCH_TEXT* SCH_TINYCAD_PLUGIN::loadLabel( wxXmlNode*         aLabelNode,
                                         std::set<wxString> aHierarchicalPins )
{
    SCH_TEXT* label;
    bool      global = false;

    wxString labelText = aLabelNode->GetNodeContent();

    if( aHierarchicalPins.find( labelText ) != aHierarchicalPins.end() )
    {
        label = new SCH_HIERLABEL();
    }
    else
    {
        switch( wxAtoi( aLabelNode->GetAttribute( "style" ) ) )
        {
        case 0:
        default:
            label = new SCH_LABEL();
            global = false;
            break;
        case 1:
            label = new SCH_GLOBALLABEL();
            label->SetShape( PINSHEETLABEL_SHAPE::PS_INPUT );
            global = true;
            break;
        case 2:
            label = new SCH_GLOBALLABEL();
            label->SetShape( PINSHEETLABEL_SHAPE::PS_OUTPUT );
            global = true;
            break;
        case 3:
            label = new SCH_GLOBALLABEL();
            label->SetShape( PINSHEETLABEL_SHAPE::PS_BIDI );
            global = true;
            break;
        }
    }

    int              dir = wxAtoi( aLabelNode->GetAttribute( "direction" ) );
    LABEL_SPIN_STYLE rotation;
    switch( dir )
    {
    default: rotation = global ? LABEL_SPIN_STYLE::RIGHT : LABEL_SPIN_STYLE::LEFT; break;
    case 0: rotation = global ? LABEL_SPIN_STYLE::BOTTOM : LABEL_SPIN_STYLE::UP; break;
    case 1: rotation = global ? LABEL_SPIN_STYLE::UP : LABEL_SPIN_STYLE::BOTTOM; break;
    case 2: rotation = global ? LABEL_SPIN_STYLE::RIGHT : LABEL_SPIN_STYLE::LEFT; break;
    case 3: rotation = global ? LABEL_SPIN_STYLE::LEFT : LABEL_SPIN_STYLE::RIGHT; break;
    }
    label->SetLabelSpinStyle( rotation );


    label->SetText( labelText );
    label->SetPosition( parsePosition( aLabelNode->GetAttribute( "pos" ) ) );
    label->SetVisible( true );
    return label;
}

SCH_TEXT* SCH_TINYCAD_PLUGIN::loadPowerLabel( wxXmlNode* aPowerLabelNode )
{
    SCH_TEXT* powerLabel;
    powerLabel = new SCH_GLOBALLABEL();
    powerLabel->SetShape( PINSHEETLABEL_SHAPE::PS_UNSPECIFIED );

    switch( wxAtoi( aPowerLabelNode->GetAttribute( "direction" ) ) )
    {
    default: powerLabel->SetLabelSpinStyle( LABEL_SPIN_STYLE::LEFT ); break;
    case 0: powerLabel->SetLabelSpinStyle( LABEL_SPIN_STYLE::UP ); break;
    case 1: powerLabel->SetLabelSpinStyle( LABEL_SPIN_STYLE::BOTTOM ); break;
    case 2: powerLabel->SetLabelSpinStyle( LABEL_SPIN_STYLE::LEFT ); break;
    case 3: powerLabel->SetLabelSpinStyle( LABEL_SPIN_STYLE::RIGHT ); break;
    }

    powerLabel->SetText( aPowerLabelNode->GetNodeContent() );
    powerLabel->SetPosition( parsePosition( aPowerLabelNode->GetAttribute( "pos" ) ) );
    powerLabel->SetVisible( true );
    return powerLabel;
}

SCH_JUNCTION* SCH_TINYCAD_PLUGIN::loadJunction( wxXmlNode* aJunctionNode )
{
    SCH_JUNCTION* junction = new SCH_JUNCTION();
    junction->SetPosition( parsePosition( aJunctionNode->GetAttribute( "pos" ) ) );
    return junction;
}

bool SCH_TINYCAD_SYMBOL_PARSER::LoadSymbol( wxXmlNode* aSymbolNode, SCH_TINYCAD_SYMBOL* aPart,
                                            std::map<int, textFont*>*  aTextFontMap,
                                            std::map<int, lineStyle*>* aLineStyleMap,
                                            bool aLibFormat, wxXmlNode* aDetailsNode )
{
    if( aTextFontMap == nullptr )
    {
        aTextFontMap = new std::map<int, textFont*>;
    }

    if( aLineStyleMap == nullptr )
    {
        aLineStyleMap = new std::map<int, lineStyle*>;
    }

    NODE_MAP        symbolChildren = MapChildren( aSymbolNode );
    NODE_VECTOR_MAP symbolChildrenList = MapVectorChildren( aSymbolNode );
    LIB_SYMBOL*     symbol = aPart->m_symbol;

    wxString symbolName;
    wxString symbolDescription;
    // stuff inside these if statements should be moved outside.
    if( aDetailsNode )
    {
        NODE_VECTOR_MAP detailsChildren = MapVectorChildren( aDetailsNode );
        symbolName = detailsChildren["NAME"][0]->GetNodeContent();
        symbolDescription = detailsChildren["DESCRIPTION"][0]->GetNodeContent();
        symbol->SetUnitCount( wxAtoi( symbolChildrenList["PPP"][0]->GetNodeContent() ) );
        loadFieldAttributes( symbol, detailsChildren["FIELD"] );
        symbol->GetReferenceField().SetText( detailsChildren["REF"][0]->GetNodeContent() );
    }
    else if( aLibFormat )
    {
        NODE_VECTOR_MAP detailsChildren = MapVectorChildren( symbolChildrenList["DETAILS"][0] );
        symbolName = detailsChildren["NAME"][0]->GetNodeContent();
        symbolDescription = detailsChildren["DESCRIPTION"][0]->GetNodeContent();
        symbol->SetUnitCount( wxAtoi( detailsChildren["PPP"][0]->GetNodeContent() ) );
        loadFieldAttributes( symbol, detailsChildren["FIELD"] );
    }
    else
    {
        symbolName = symbolChildren["NAME"]->GetNodeContent();
        symbolDescription = symbolChildren["DESCRIPTION"]->GetNodeContent();
        symbol->SetUnitCount( wxAtoi( symbolChildren["PPP"]->GetNodeContent() ) );
        loadFieldAttributes( symbol, symbolChildrenList["FIELD"] );
    }
    symbolName = EscapeString( symbolName, ESCAPE_CONTEXT::CTX_NETNAME );
    std::cout << "Parsing symbol: " << symbolName << std::endl;
    // add other info fields such as link and package

    //wxXmlNode* symbolSheet;
    std::vector<wxXmlNode*> symbols;

    if( symbolChildren["TinyCADSheets"] )
    {
        //symbolSheet = symbolChildren["TinyCADSheets"]->GetChildren();
        symbols = MapVectorChildren( symbolChildren["TinyCADSheets"] )["TinyCAD"];
    }
    else if( symbolChildren["TinyCAD"] )
    {
        symbols.push_back( symbolChildren["TinyCAD"] );
        //symbolSheet = symbolChildren["TinyCAD"];
    }


    // update this to calculate correct unit count
    symbol->SetName( symbolName );
    symbol->SetDescription( symbolDescription );
    //symbolPart->SetName ("symbol name placeholder");

    //m pi 1446


    bool useOuterUnit = symbols.size() > 1;

    for( wxXmlNode* refNode : symbolChildrenList["REF_POINT"])
    {
        int unit = wxAtoi(refNode->GetAttribute("part")) + 1;
        aPart->SetREF_POINT(
            parseSymbolPosition(refNode->GetAttribute("pos")), unit);
        std::cout << "Set initial ref point: " << unit << std::endl;
    }

    int showPinNameAndNr = -1;

    symbol->LockUnits( useOuterUnit );
    int unit = 1;
    for( wxXmlNode* symbolNode : symbols )
    {
        //NODE_MAP symbolSheetChildren = MapChildren( symbol );
        wxXmlNode* currentNode = symbolNode->GetChildren();

        while( currentNode )
        {
            wxString  nodeName = currentNode->GetName();
            LIB_ITEM* drawItem = nullptr;

            if( nodeName == "FONT" )
            {
                ( *aTextFontMap )[wxAtoi( currentNode->GetAttribute( "id" ) )] = LoadTextFont(currentNode);
            }

            if( nodeName == "STYLE" )
            {
                NODE_VECTOR_MAP styleChildrenList = MapVectorChildren( currentNode );
                lineStyle*      ls = new lineStyle();

                int thickness = wxAtoi( styleChildrenList["THICKNESS"][0]->GetNodeContent() );
                ls->width = thickness * 1270;

                ( *aLineStyleMap )[wxAtoi( currentNode->GetAttribute( "id" ) )] = ls;
            }
            else if( nodeName == "ORIGIN" )
            {
            }
            else if( nodeName == "PIN" )
            {
                drawItem = loadPin( aPart, currentNode );

                // Let the first pin of the symbol decide if pin names and numbers shall be shown or not
                if( showPinNameAndNr < 0 )
                {
                    showPinNameAndNr = wxAtoi( currentNode->GetAttribute( "show" ) );
                }

                //symbol->AddDrawItem( pin );
                //symbol->AddDrawItem(drawItem);
            }
            else if( nodeName == "POLYGON" )
            {
                int unitToUse = useOuterUnit ? unit : 0;
                for( LIB_ITEM* polygon : loadPolygon( aPart, currentNode, aLineStyleMap ) )
                {
                    polygon->SetUnit( unitToUse );
                    symbol->AddDrawItem( polygon );
                }
                aPart->SetBottomRight( aPart->m_temp_bottomRight, unitToUse );
                aPart->SetTopLeft( aPart->m_temp_topLeft, unitToUse );
            }
            else if( nodeName == "RECTANGLE" )
            {
                drawItem = loadRectangle( aPart, currentNode, aLineStyleMap );
            }
            else if( nodeName == "TEXT" )
            {
                drawItem = loadText( aPart, currentNode, aTextFontMap );
            }
            else if( nodeName == "ELLIPSE" )
            {
                drawItem = loadEllipse( aPart, currentNode, aLineStyleMap );
            }

            if( drawItem )
            {
                int unitToUse = 0;
                if( useOuterUnit )
                {
                    unitToUse = unit;
                }
                else if( drawItem->Type() == KICAD_T::LIB_PIN_T )
                {
                    unitToUse = wxAtoi( currentNode->GetAttribute( "part" ) ) + 1;
                }
                drawItem->SetUnit( unitToUse );

                if( drawItem->Type() != KICAD_T::LIB_TEXT_T )
                {
                    aPart->SetBottomRight( aPart->m_temp_bottomRight, unitToUse );
                    aPart->SetTopLeft( aPart->m_temp_topLeft, unitToUse );
                }

                symbol->AddDrawItem( drawItem );
            }

            currentNode = currentNode->GetNext();
        }

        unit++;
    }
    aPart->AlignSymbol( ALIGNMENT::CENTER );

    switch( showPinNameAndNr )
    {
    default:
        symbol->SetShowPinNames( true );
        symbol->SetShowPinNumbers( true );
    case 3:
        symbol->SetShowPinNames( true );
        symbol->SetShowPinNumbers( true );
        break;
    case 2:
        symbol->SetShowPinNames( false );
        symbol->SetShowPinNumbers( true );
        break;
    case 1:
        symbol->SetShowPinNames( true );
        symbol->SetShowPinNumbers( false );
        break;
    case 0:
        symbol->SetShowPinNames( false );
        symbol->SetShowPinNumbers( false );
        break;
    }

    std::cout << "Saved symbol to library" << std::endl;

    return true;
}

void SCH_TINYCAD_SYMBOL_PARSER::alignSymbol( LIB_SYMBOL* aPart, ALIGNMENT aAlignment )
{
    wxPoint offset;
    bool    isInterchangable = !aPart->UnitsLocked();
    if( isInterchangable )
    {
        offset = GetAlignmentOffset( aPart, 0, aAlignment );
    }

    for( LIB_SYMBOL_UNITS unit : aPart->GetUnitDrawItems() )
    {
        if( !isInterchangable )
            offset = GetAlignmentOffset( aPart, unit.m_unit, aAlignment );

        for( LIB_ITEM* item : unit.m_items )
        {
            item->Offset( wxPoint( offset ) );
        }
    }
    snapPins( aPart );
}

wxPoint SCH_TINYCAD_SYMBOL_PARSER::GetAlignmentOffset( LIB_SYMBOL* aPart, int aUnit,
                                                       ALIGNMENT aAlignment )
{
    wxPoint offset;
    if( aAlignment == ALIGNMENT::CENTER )
    {
        wxPoint center = getCenter( aPart, aUnit );
        offset = wxPoint( -center.x, center.y );
    }
    else if( aAlignment == ALIGNMENT::BOTTOMRIGHT )
    {
        wxPoint bottomRight = getBottomRight( aPart, aUnit );
        offset = wxPoint( -bottomRight.x, bottomRight.y );
    }
    return offset;
}

wxString getFieldName( wxString aName )
{
    if( aName == "Ref" )
    {
        return "Reference";
    }
    else if( aName == "Name" )
    {
        return "Value";
    }
    else if( aName == "Package" )
    {
        return "Footprint";
    }
    else if( aName == "Value" )
    {
        return "Val";
    }
    else
    {
        return aName;
    }
}

wxString getFieldValue( wxString aValue )
{
    return aValue;
}

void SCH_TINYCAD_PLUGIN::loadFieldAttributes( SCH_SYMBOL*             aComponent,
                                              std::vector<wxXmlNode*> aFields )
{
    bool       foundField;
    SCH_FIELD* field;
    wxString   fieldName;
    wxString   fieldValue;
    for( wxXmlNode* fieldNode : aFields )
    {
        fieldName = getFieldName( fieldNode->GetAttribute( "description" ) );
        fieldValue = getFieldValue( fieldNode->GetAttribute( "value" ) );
        field = aComponent->FindField( fieldName );
        foundField = !( field == NULL );
        if( !foundField )
        {
            int fieldId = aComponent->GetFieldCount();

            field = new SCH_FIELD( parseSymbolPosition( fieldNode->GetAttribute( "pos" ) )
                                           + aComponent->GetPosition(),
                                   fieldId, aComponent, fieldName );
        }
        else
        {
            field->SetTextPos( parseSymbolPosition( fieldNode->GetAttribute( "pos" ) )
                               + aComponent->GetPosition() );
        }
        field->SetHorizJustify( GR_TEXT_HJUSTIFY_LEFT );
        field->SetText( fieldValue );
        field->SetVisible( wxAtoi( fieldNode->GetAttribute( "show" ) ) == 1 );

        if( !foundField )
        {
            aComponent->AddField( *field );
        }
    }
}

void SCH_TINYCAD_SYMBOL_PARSER::loadFieldAttributes( LIB_SYMBOL*             aPart,
                                                     std::vector<wxXmlNode*> aFields )
{
    bool       foundField;
    LIB_FIELD* field;
    NODE_MAP   fieldChildren;
    wxString   fieldName;
    wxString   fieldValue;
    for( wxXmlNode* fieldNode : aFields )
    {
        fieldChildren = MapChildren( fieldNode );
        fieldName = getFieldName( fieldChildren["NAME"]->GetNodeContent() );

        fieldValue = getFieldValue( fieldChildren["VALUE"] != NULL
                                            ? fieldChildren["VALUE"]->GetNodeContent()
                                            : fieldChildren["DEFAULT"]->GetNodeContent() );

        field = aPart->FindField( fieldName );
        foundField = !( field == NULL );
        if( !foundField )
        {
            int fieldId = aPart->GetFieldCount();

            field = new LIB_FIELD( aPart, fieldId );
            field->SetName( fieldName );
        }

        field->SetHorizJustify( GR_TEXT_HJUSTIFY_LEFT );
        // should not hard code
        field->SetPosition( wxPoint( 0, 0 ) );
        field->SetText( fieldValue );

        if( !foundField )
        {
            aPart->AddField( field );
        }
    }
}

LIB_PIN* SCH_TINYCAD_SYMBOL_PARSER::loadPin( SCH_TINYCAD_SYMBOL* aPart, wxXmlNode* aPinNode )
{
    LIB_PIN* pin( new LIB_PIN( aPart->m_symbol ) );
    int      pinLength = convertToEeschemaUnits( wxAtoi( aPinNode->GetAttribute( "length" ) ) ) / 5;
    pin->SetName( aPinNode->GetNodeContent() );

    pin->SetNumber( aPinNode->GetAttribute( "number" ) );

    wxPoint pos = parseSymbolPosition( aPinNode->GetAttribute( "pos" ) );
    int     dir = wxAtoi( aPinNode->GetAttribute( "direction" ) );
    switch( dir )
    {
    default: wxASSERT_MSG( false, wxString::Format( "Unhandled orientation" ) ); KI_FALLTHROUGH;
    case 0:
        pin->SetOrientation( 'U' );
        break;
    case 1:
        pin->SetOrientation( 'D' );
        break;
    case 2:
        pin->SetOrientation( 'L' );
        break;
    case 3:
        pin->SetOrientation( 'R' );
        break;
    }

    switch( wxAtoi( aPinNode->GetAttribute( "elec" ) ) )
    {
    default: pin->SetType( ELECTRICAL_PINTYPE::PT_INPUT ); break;
    case 0: pin->SetType( ELECTRICAL_PINTYPE::PT_INPUT ); break;
    case 1: pin->SetType( ELECTRICAL_PINTYPE::PT_OUTPUT ); break;
    case 2: pin->SetType( ELECTRICAL_PINTYPE::PT_TRISTATE ); break;
    case 3: pin->SetType( ELECTRICAL_PINTYPE::PT_OPENCOLLECTOR ); break;
    case 4: pin->SetType( ELECTRICAL_PINTYPE::PT_PASSIVE ); break;
    case 5: pin->SetType( ELECTRICAL_PINTYPE::PT_BIDI ); break;
    case 6: pin->SetType( ELECTRICAL_PINTYPE::PT_NC ); break;
    }

    switch( wxAtoi( aPinNode->GetAttribute( "which" ) ) )
    {
    default: pin->SetShape( GRAPHIC_PINSHAPE::LINE ); break;
    case 0: pin->SetShape( GRAPHIC_PINSHAPE::LINE ); break;
    case 1: pin->SetShape( GRAPHIC_PINSHAPE::INVERTED ); break;
    case 2: pin->SetShape( GRAPHIC_PINSHAPE::CLOCK ); break;
    case 3: pin->SetShape( GRAPHIC_PINSHAPE::INVERTED_CLOCK ); break;
    case 4: pin->SetShape( GRAPHIC_PINSHAPE::LINE ); break;
    case 5: pin->SetShape( GRAPHIC_PINSHAPE::LINE ); break;
    case 6: pin->SetShape( GRAPHIC_PINSHAPE::NONLOGIC ); break;
    }


    // TODO implement all cases
    // SetNameTextSize
    // SetNumberTextSize
    //
    switch( wxAtoi( aPinNode->GetAttribute( "show" ) ) )
    {
    default: pin->SetVisible( true ); break;
    //case 0:
    case 2: pin->SetNameTextSize( 0 );
    }

    aPart->m_temp_bottomRight = pos;
    aPart->m_temp_topLeft = pos;
    pin->SetPosition( pos );

    pin->SetLength( pinLength );

    return pin;
}

LIB_TEXT* SCH_TINYCAD_SYMBOL_PARSER::loadText( SCH_TINYCAD_SYMBOL* aPart, wxXmlNode* aTextNode,
                                               std::map<int, textFont*>* aTextFontMap )
{
    LIB_TEXT* text( new LIB_TEXT( aPart->m_symbol ) );
    text->SetText( aTextNode->GetNodeContent() );
    text->SetPosition( parseSymbolPosition( aTextNode->GetAttribute( "pos" ) ) );

    if( aTextNode->GetAttribute( "direction" ) == '0' )
    {
        text->SetTextAngle( 900.0 );
    }
    text->SetHorizJustify( EDA_TEXT_HJUSTIFY_T::GR_TEXT_HJUSTIFY_LEFT );
    text->SetVertJustify( EDA_TEXT_VJUSTIFY_T::GR_TEXT_VJUSTIFY_BOTTOM );

    int id = wxAtoi( aTextNode->GetAttribute( "font" ) );

    if( ( *aTextFontMap ).count( id ) )
    {
        int size = ( *aTextFontMap )[id]->size;
        text->SetTextSize( wxSize( size, size ) );
        text->SetBold( ( *aTextFontMap )[id]->bold );
        text->SetItalic( ( *aTextFontMap )[id]->italic );
    }

    return text;
}

LIB_CIRCLE* SCH_TINYCAD_SYMBOL_PARSER::loadEllipse( SCH_TINYCAD_SYMBOL*        aPart,
                                                    wxXmlNode*                 aEllipseNode,
                                                    std::map<int, lineStyle*>* aLineStyleMap )
{
    LIB_CIRCLE* circle = new LIB_CIRCLE( aPart->m_symbol );
    // corner positions in tinycad
    wxPoint pointA = parseSymbolPosition( aEllipseNode->GetAttribute( "a" ) );
    wxPoint pointB = parseSymbolPosition( aEllipseNode->GetAttribute( "b" ) );
    int     r1 = std::abs( pointA.x - pointB.x ) / 2;
    int     r2 = std::abs( pointA.y - pointB.y ) / 2;
    // estimate circle radius
    int     r = ( r1 + r2 ) / 2;
    wxPoint center = wxPoint( ( pointA.x + pointB.x ) / 2, ( pointA.y + pointB.y ) / 2 );
    circle->SetPosition( center );
    circle->SetRadius( r );

    int id = wxAtoi( aEllipseNode->GetAttribute( "style" ) );

    if( ( *aLineStyleMap ).count( id ) )
    {
        int size = ( *aLineStyleMap )[id]->width;
        circle->SetWidth( size );
    }

    if( wxAtoi( aEllipseNode->GetAttribute( "fill" ) ) > 0 )
    {
        circle->SetFillMode( FILL_TYPE::FILLED_SHAPE );
    }
    else
    {
        circle->SetFillMode( FILL_TYPE::FILLED_WITH_BG_BODYCOLOR );
    }

    aPart->m_temp_bottomRight =
            wxPoint( std::max( pointA.x, pointB.x ), std::min( pointA.y, pointB.y ) );
    aPart->m_temp_topLeft =
            wxPoint( std::min( pointA.x, pointB.x ), std::max( pointA.y, pointB.y ) );

    return circle;
}

LIB_RECTANGLE* SCH_TINYCAD_SYMBOL_PARSER::loadRectangle( SCH_TINYCAD_SYMBOL*        aPart,
                                                         wxXmlNode*                 aRectNode,
                                                         std::map<int, lineStyle*>* aLineStyleMap )
{
    LIB_RECTANGLE* rectangle( new LIB_RECTANGLE( aPart->m_symbol ) );

    wxPoint pointA = parseSymbolPosition( aRectNode->GetAttribute( "a" ) );
    wxPoint pointB = parseSymbolPosition( aRectNode->GetAttribute( "b" ) );
    rectangle->SetPosition( pointA );
    rectangle->SetEnd( pointB );

    int id = wxAtoi( aRectNode->GetAttribute( "style" ) );

    if( ( *aLineStyleMap ).count( id ) )
    {
        int size = ( *aLineStyleMap )[id]->width;
        rectangle->SetWidth( size );
    }

    if( wxAtoi( aRectNode->GetAttribute( "fill" ) ) > 0 )
    {
        rectangle->SetFillMode( FILL_TYPE::FILLED_SHAPE );
    }
    else
    {
        rectangle->SetFillMode( FILL_TYPE::FILLED_WITH_BG_BODYCOLOR );
    }

    aPart->m_temp_bottomRight =
            wxPoint( std::max( pointA.x, pointB.x ), std::min( pointA.y, pointB.y ) );
    aPart->m_temp_topLeft =
            wxPoint( std::min( pointA.x, pointB.x ), std::max( pointA.y, pointB.y ) );

    return rectangle;
}
std::vector<LIB_ITEM*>
SCH_TINYCAD_SYMBOL_PARSER::loadPolygon( SCH_TINYCAD_SYMBOL* aPart, wxXmlNode* aPolygonNode,
                                        std::map<int, lineStyle*>* aLineStyleMap )
{
    wxPoint bottomRight = wxPoint( 0, 0 );
    wxPoint topLeft = wxPoint( 0, 0 );

    wxPoint pos = parseSymbolPosition( aPolygonNode->GetAttribute( "pos" ) );

    std::vector<LIB_ITEM*> polygons;

    wxXmlNode* currentVertex = aPolygonNode->GetChildren();
    wxXmlNode* prevVertex = nullptr;

    while( currentVertex )
    {
        std::vector<wxXmlNode*> polyLineVertices;

        wxPoint offset = parseSymbolPosition( currentVertex->GetAttribute( "pos" ) );
        while( currentVertex && wxAtoi( currentVertex->GetAttribute( "arc" ) ) == 0 )
        {
            if( prevVertex )
                polyLineVertices.push_back( prevVertex );
            polyLineVertices.push_back( currentVertex );

            prevVertex = currentVertex;
            currentVertex = currentVertex->GetNext();
        }
        if( polyLineVertices.size() > 1 )
            polygons.push_back( loadPolyline( aPart, pos, polyLineVertices ) );
        while( currentVertex && wxAtoi( currentVertex->GetAttribute( "arc" ) ) != 0 )
        {
            wxPoint prevPt = parseSymbolPosition( prevVertex->GetAttribute( "pos" ) );
            wxPoint pt = parseSymbolPosition( currentVertex->GetAttribute( "pos" ) );
            if( prevPt.x == pt.x || prevPt.y == pt.y )
            {
                polygons.push_back( loadPolyline(
                        aPart, pos, std::vector<wxXmlNode*>( { prevVertex, currentVertex } ) ) );
            }
            else
            {
                polygons.push_back( loadArc( aPart, pos, prevVertex, currentVertex ) );
            }

            prevVertex = currentVertex;
            currentVertex = currentVertex->GetNext();
        }
    }

    NODE_VECTOR_MAP vertices = MapVectorChildren( aPolygonNode );
    for( wxXmlNode* vertex : vertices["POINT"] )
    {
        wxPoint pt = parseSymbolPosition( vertex->GetAttribute( "pos" ) );
        if( pt.x > bottomRight.x )
            bottomRight.x = pt.x;
        if( pt.y < bottomRight.y )
            bottomRight.y = pt.y;

        if( pt.x < topLeft.x )
            topLeft.x = pt.x;
        if( pt.y > topLeft.y )
            topLeft.y = pt.y;
    }

    int id = wxAtoi( aPolygonNode->GetAttribute( "style" ) );
    for( LIB_ITEM* polygon : polygons )
    {
        if( ( *aLineStyleMap ).count( id ) )
        {
            int size = ( *aLineStyleMap )[id]->width;
            polygon->SetWidth( size );
        }

        if( wxAtoi( aPolygonNode->GetAttribute( "fill" ) ) > 0 )
        {
            polygon->SetFillMode( FILL_TYPE::FILLED_SHAPE );
        }
        else
        {
            static KICAD_T polygonType[] = { LIB_POLYLINE_T };
            if( polygon->IsType( polygonType ) )
            {
                polygon->SetFillMode( FILL_TYPE::FILLED_WITH_BG_BODYCOLOR );
            }
        }
    }
    aPart->m_temp_bottomRight = pos + bottomRight;
    aPart->m_temp_topLeft = pos + topLeft;
    return polygons;
}
LIB_POLYLINE* SCH_TINYCAD_SYMBOL_PARSER::loadPolyline( SCH_TINYCAD_SYMBOL* aPart, wxPoint aPos,
                                                       std::vector<wxXmlNode*> aVertices )
{
    LIB_POLYLINE* polyLine( new LIB_POLYLINE( aPart->m_symbol ) );
    wxPoint       offset = parseSymbolPosition( aVertices[0]->GetAttribute( "pos" ) );
    for( wxXmlNode* vertex : aVertices )
    {
        polyLine->AddPoint( parseSymbolPosition( vertex->GetAttribute( "pos" ) ) - offset );
    }
    polyLine->SetPosition( aPos + offset );
    return polyLine;
}

LIB_ARC* SCH_TINYCAD_SYMBOL_PARSER::loadArc( SCH_TINYCAD_SYMBOL* aPart, wxPoint aPos,
                                             wxXmlNode* aArcNodeA, wxXmlNode* aArcNodeB )
{
    LIB_ARC* arc( new LIB_ARC( aPart->m_symbol ) );
    wxPoint  arcStart, arcEnd;
    arcStart = parseSymbolPosition( aArcNodeA->GetAttribute( "pos" ) );
    arcEnd = parseSymbolPosition( aArcNodeB->GetAttribute( "pos" ) );
    int curve = wxAtoi( aArcNodeB->GetAttribute( "arc" ) );

    int width = std::abs( arcEnd.x - arcStart.x );
    int height = std::abs( arcEnd.y - arcStart.y );

    // all corners of the bounding box of the arc
    // get top left corner
    wxPoint p1 = wxPoint( std::min( arcStart.x, arcEnd.x ), std::max( arcStart.y, arcEnd.y ) );
    wxPoint p2 = p1 + wxPoint( width, 0 );
    wxPoint p3 = p1 + wxPoint( 0, -height );
    wxPoint p4 = p1 + wxPoint( width, -height );

    int oppositeCurve = curve == 1 ? 2 : 1;

    curve = arcStart.x > arcEnd.x ? oppositeCurve : curve;

    wxPoint center;
    if( p1 == arcStart || p1 == arcEnd )
    {
        // use p2 or p3 as center
        if( curve == 1 )
            center = p3;
        if( curve == 2 )
            center = p2;
    }
    else
    {
        // use p1 or p4 as center
        if( curve == 1 )
            center = p1;
        if( curve == 2 )
            center = p4;
    }

    arc->SetPosition( center + aPos );
    arc->SetStart( arcStart + aPos );
    arc->SetEnd( arcEnd + aPos );

    arc->CalcRadiusAngles();
    return arc;
}

textFont* SCH_TINYCAD_SYMBOL_PARSER::LoadTextFont( wxXmlNode* aTextFontNode )
{
    NODE_VECTOR_MAP fontChildrenList = MapVectorChildren( aTextFontNode );
    textFont*       tf = new textFont();

    int      fontScale = 3300; // 3300 is a suitable value for most fonts
    wxString fontName = fontChildrenList["FACENAME"][0]->GetNodeContent();

    if( fontName == "Calibri" )
    {
        fontScale = 2550;
    }
    else if( fontName == "Times New Roman" )
    {
        fontScale = 3450;
    }
    else if( fontName == "Comic Sans MS" )
    {
        fontScale = 3200;
    }

    if( wxAtoi( fontChildrenList["WEIGHT"][0]->GetNodeContent() ) < 500 )
    {
        tf->bold = false;
    }
    else
    {
        tf->bold = true;
        fontScale = fontScale - 50; // Bold texts are a bit bigger so lower the size a little bit
    }

    if( wxAtoi( fontChildrenList["ITALIC"][0]->GetNodeContent() ) < 100 )
    {
        tf->italic = false;
    }
    else
    {
        tf->italic = true;
    }

    tf->size = wxAtoi( fontChildrenList["WIDTH"][0]->GetNodeContent() ) * fontScale;
    return tf;
}

void SCH_TINYCAD_SYMBOL_PARSER::snapPins( LIB_SYMBOL* aPart )
{
    for( LIB_SYMBOL_UNITS unit : aPart->GetUnitDrawItems() )
    {
        LIB_PINS pins;
        aPart->GetPins( pins );
        int yoffset = 0, xoffset = 0;
        for( LIB_PIN* pin : pins )
        {
            wxPoint pos = pin->GetPosition();

            int orientation = pin->GetOrientation();
            switch( orientation )
            {
            case 'U':
            case 'D': xoffset = roundToGrid( pos.x ) - pos.x; break;
            case 'L':
            case 'R': yoffset = roundToGrid( pos.y ) - pos.y; break;
            }
        }
        if( xoffset < 0 )
            xoffset += convertToEeschemaUnits( 2 );

        if( yoffset < 0 )
            yoffset += convertToEeschemaUnits( 2 );

        for( LIB_ITEM* item : unit.m_items )
        {
            item->Offset( wxPoint( xoffset, yoffset ) );
        }
    }
}

wxPoint SCH_TINYCAD_SYMBOL_PARSER::getCenter( LIB_SYMBOL* aPart, int aUnit )
{
    return roundToGrid( aPart->GetBodyBoundingBox( aUnit, 0 ).GetCenter() );
}

wxPoint SCH_TINYCAD_SYMBOL_PARSER::getBottomRight( LIB_SYMBOL* aPart, int aUnit )
{
    EDA_RECT bbox = aPart->GetBodyBoundingBox( aUnit, 0 );
    wxPoint  bottomRight = wxPoint( bbox.GetRight(), bbox.GetBottom() );
    return roundToGrid( bottomRight );
}

wxPoint roundToGrid( wxPoint aPoint )
{
    return wxPoint( roundToGrid( aPoint.x ), roundToGrid( aPoint.y ) );
}

int roundToGrid( int aValue )
{
    int sign = aValue < 0 ? -1 : 1;
    int gridSize = convertToEeschemaUnits( 2 );
    int remainder = aValue % gridSize;
    // small error boundary (size of overflowing pin roundness is 3810)
    // round to custom increment:
    // round(val / increment) * increment
    if( std::abs( remainder ) < 0 )
    {
        // rounds down because of standard integer division
        return sign * ( std::abs( aValue ) / gridSize ) * gridSize;
    }
    else
    {
        // rounds up by using formula:
        // (int1 + int2 - 1) / int2
        return sign * ( ( std::abs( aValue ) + gridSize - 1 ) / gridSize ) * gridSize;
    }
}

int convertToEeschemaUnits( int aValue )
{
    return aValue * 10000 * 1.27;
}

int convertToEeschemaUnits( float aValue )
{
    return aValue * 10000 * 1.27;
}

wxPoint parseSymbolPosition( wxString aPos )
{
    wxPoint pos = parsePosition( aPos );
    return wxPoint( pos.x, -pos.y );
}
wxPoint parseSymbolPosition( wxString aPos, wxPoint aCenterPos )
{
    wxPoint pos = parsePosition( aPos );
    return wxPoint( ( pos.x - aCenterPos.x ), -( pos.y - aCenterPos.y ) );
}

wxPoint parseSymbolPosition( wxString aPos, wxString aCenterPos )
{
    wxPoint pos = parsePosition( aPos );
    wxPoint centerPos = parsePosition( aCenterPos );
    return wxPoint( ( pos.x - centerPos.x ), -( pos.y - centerPos.y ) );
}

wxPoint parsePosition( wxString aPos )
{
    vector<string> pos;
    boost::split( pos, aPos, boost::is_any_of( "," ) );
    return wxPoint( convertToEeschemaUnits( std::stof( pos[0] ) ),
                    convertToEeschemaUnits( std::stof( pos[1] ) ) );
}

bool SCH_TINYCAD_PLUGIN::CheckHeader( const wxString& aFileName )
{
    wxTextFile tempFile;

    tempFile.Open( aFileName );
    wxString firstline;

    firstline = tempFile.GetFirstLine();
    wxString secondline = tempFile.GetNextLine();
    wxString thirdline = tempFile.GetNextLine();
    tempFile.Close();

    return firstline.StartsWith( "<?xml" );
}
